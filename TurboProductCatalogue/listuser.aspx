﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="listuser.aspx.cs" MasterPageFile="~/Masters/Site.Master" Inherits="TurboProductCatalogue.listuser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="content-header">
        <h1>
            <i class="fa fa-user "></i>&nbsp;List of Users
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of Users</h3>
                        <div class="box-tools">
                            <a class="btn btn-primary btn-sm" href="createuser.aspx" role="button">Create User</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <tr class="info">
                                    <th class="text-center" style="width: 5%">#</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center" style="width: 25%">Email-ID</th>
                                    <th class="text-center" style="width: 15%">Mobile No.</th>
                                    <th class="text-center" style="width: 15%">Action</th>
                                </tr>

                                <tr>
                                    <td class="text-center">1</td>
                                    <td>Suresh</td>
                                    <td>sureshbabu.ask@gmail.com</td>
                                    <td class="text-center">7904768050</td>

                                    <td class="text-center">
                                        <a title="Edit" style="padding-right:15px" href="#"><i class="fa fa-edit"></i></a>
                                        &nbsp;&nbsp;<a onclick="javascript:return confirm(&quot;Are you sure you want to delete?&quot;);"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>


</asp:Content>
