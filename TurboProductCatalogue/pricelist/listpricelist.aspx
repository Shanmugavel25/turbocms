﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Masters/Site.Master" CodeBehind="listpricelist.aspx.cs" Inherits="TurboProductCatalogue.pricelist.listpricelist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   
 <style type="text/css">
     #pagingDiv, #pagingDiv span, #pagingDiv a 
     {
     margin:10px;
     }
     th, td 
     {
         padding:10px;
     }

 </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="content-header">
        <h1>
            <i class="fa fa-credit-card "></i>&nbsp;Price List
        </h1>
    </section>   
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pnlMSG" runat="server" Visible="false">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblMSG" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>

                 <div class="well">
                    <div class="row">
                        <div class="col-md-4 ">
                            <asp:TextBox runat="server" ID="txt_search" placeholder="Type Something ..." CssClass="form-control" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="req" runat="server" ValidationGroup="btnSearch" ErrorMessage="Type Something ..." ControlToValidate="txt_search" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                        </div>
                        <div class="col-md-4 ">
                            <asp:Button class="btn btn-primary" ID="Button1" ValidationGroup="btnSearch" Text="Search" runat="server" OnClick="btn_submit_Click" />
                        </div>
                    </div>
                </div>

                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of Price</h3>
                        <div class="box-tools">
                            <a href="uploadpricelist.aspx" class="btn btn-success btn-sm">Upload Price</a>
                        </div>
                    </div>
                    <asp:ScriptManager ID="ScriptManager1" runat="server" />
                    <div class="box-body">
                        <div class="table-responsive">
                      
                            <asp:Repeater ID="rptPriceList" runat="server" OnItemCommand="rptPriceList_ItemCommand" >
                                <HeaderTemplate>
                                    <table id="tblCustomers" class="table table-hover table-bordered">
                                        <tr class="success">
                                            <th class="text-center" style="width: 5%">S.No</th>
                                            <th class="text-center" style="width: 20%">Old Part No</th>
                                            <th class="text-center" style="width: 20%">New Part No</th>
                                            <th class="text-center" style="width: 20%">Description</th>
                                            <th class="text-center" style="width: 10%">List Price</th>
                                            <th class="text-center" style="width: 10%">MRP</th>
                                            <th class="text-center" style="width: 20%">Action</th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                 
                                     <tr>
                                        <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"slNo")%>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"oldpartno")%>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"newpartno")%>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"description")%>
                                        </td>
                                        <td class="text-right">
                                            ₹ <%#  DataBinder.Eval(Container.DataItem,"listprice")%>
                                        </td>
                                        <td class="text-right">
                           <%--<asp:Label ID="Label1" runat="server"   Text='₹<%#  Eval("mrp")%>' ></asp:Label>--%>
                                            ₹ <%#  DataBinder.Eval(Container.DataItem,"mrp") %>
                                        </td>
                                        <td class="text-center">
                                            <a href="updatepricelist.aspx?mode=edit&partlistID=<%# TurboProductCatalogue.MyCrypto.GetEncryptedQueryString   (DataBinder.Eval(Container.DataItem,"partlistID").ToString())%>" title="Edit PriceList" data-toggle="tooltip" data-placement="top" class="btn btn-default btn-xs btn-warning "><i class="fa fa-edit iconColor"></i></a>
                                            <a href="viewpricelist.aspx?mode=edit&partlistID=<%# TurboProductCatalogue.MyCrypto.GetEncryptedQueryString ( DataBinder.Eval(Container.DataItem,"partlistID").ToString())%>" title="View PriceList" data-toggle="tooltip" data-placement="top" class="btn btn-primary btn-xs btn-primary "><i class="fa fa-eye iconColor"></i></a>
                                            
                                             <asp:LinkButton ID="lnkDelete" ToolTip="Delete" CommandName="delete" CommandArgument='<%# Eval("partlistID") %>'  class="btn btn-default btn-xs btn-danger" data-toggle="tooltip" data-placement="top" runat="server" Text='Delete User' OnClientClick='javascript:return confirm("Are you sure you want to delete?")'><i class="fa fa-trash iconColor"></i></asp:LinkButton>
                                        </td>

                                    </tr>
                               
  
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>

                         </asp:Repeater>

                             

                              <div class="pagination" >

                                    <asp:Repeater ID="Repeater2" runat="server" OnItemCommand="Repeater2_ItemCommand">  
                <ItemTemplate>  
                    <asp:LinkButton ID="lnkPage" 
                         onmouseover="this.style.background='Green';" onmouseout="this.style.background='white';" 
                        Style="padding: 8px; margin: 2px; background: lightgray; border: solid 1px #666; color: black; font-weight: bold"  
                        CommandName="Page" CommandArgument="<%# Container.DataItem %>" runat="server" Font-Bold="True"><%# Container.DataItem %>  
                    </asp:LinkButton>  
                </ItemTemplate>  
            </asp:Repeater>  

        <%--<asp:Repeater ID="Repeater2" runat="server" onitemcommand="Repeater2_ItemCommand">
            <ItemTemplate>
                                <asp:LinkButton ID="btnPage" 
                 onmouseover="this.style.background='Green';" onmouseout="this.style.background='white';"
                CommandName="Page" CommandArgument="<%# Container.DataItem %>"  runat="server" ForeColor="Black" Font-Bold="True"><%# Container.DataItem %>
                                </asp:LinkButton>
           </ItemTemplate>
        </asp:Repeater>--%>
        </div>

                           
                           
                           

                           <%-- <asp:Panel ID="pnlNoRecord" Visible="false" runat="server">
                                <h3 class="text-danger text-bold text-center">No Record Found</h3>
                            </asp:Panel>--%>
                        </div>
                    </div>

                

                </div>
            </div>
            
        </div>
    </section>
</asp:Content>
