﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.pricelist
{
    public partial class listpricelist : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        private int iPageSize = 50;
        public bool isSearch = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            string strMSG;
            strMSG = Request.QueryString["msg"];
            if (!IsPostBack)
            {
                this.GetPriceList(1, "selectList");
            }  
             
        }


        private void GetPriceList(int iPageIndex, string qtype)
        {
            string conString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            SqlConnection sqlCon = new SqlConnection(conString);
            sqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand("sp_b_pricelist", sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@PageIndex", iPageIndex);
            sqlCmd.Parameters.AddWithValue("@PageSize", iPageSize);
            sqlCmd.Parameters.AddWithValue("@qtype", qtype);
            sqlCmd.Parameters.AddWithValue("@search", txt_search.Text.Trim());
            sqlCmd.Parameters.Add("@RecordCount", SqlDbType.Int, 4);
            sqlCmd.Parameters["@RecordCount"].Direction = ParameterDirection.Output;
            IDataReader iDr = sqlCmd.ExecuteReader();
            rptPriceList.DataSource = iDr;
            rptPriceList.DataBind();
            iDr.Close();
            sqlCon.Close();
            int iRecordCount = Convert.ToInt32(sqlCmd.Parameters["@RecordCount"].Value);

            double dPageCount = (double)((decimal)iRecordCount / Convert.ToDecimal(iPageSize));
            int iPageCount = (int)Math.Ceiling(dPageCount);
            List<ListItem> lPages = new List<ListItem>();
            if (iPageCount > 0)
            {
                for (int i = 1; i <= iPageCount; i++)
                    lPages.Add(new ListItem(i.ToString(), i.ToString(), i != iPageIndex));
            }
            Repeater2.DataSource = lPages;
            Repeater2.DataBind();
        }  
  

        public string Set_Paging(Int32 PageNumber, int PageSize, Int64 TotalRecords, string ClassName, string PageUrl, string DisableClassName)
        {
            string ReturnValue = "";
            try
            {
                Int64 TotalPages = Convert.ToInt64(Math.Ceiling((double)TotalRecords / PageSize));
                if (PageNumber > 1)
                {
                    if (PageNumber == 2)
                        ReturnValue = ReturnValue + "<a href ='" + PageUrl.Trim() + "?pn=" + Convert.ToString(PageNumber - 1) + "' class='" + ClassName + "'></a>&nbsp&nbsp";
                    else
                    {
                        ReturnValue = ReturnValue + "<a href ='" + PageUrl.Trim();
                        if (PageUrl.Contains("?"))
                            ReturnValue = ReturnValue + "&";
                        else
                            ReturnValue = ReturnValue + "?";
                        ReturnValue = ReturnValue + "pn" + Convert.ToString(PageNumber - 1) + "' class='" + ClassName + "'>Prev</a>&nbsp&nbsp&nbsp;";


                    }
                }
                else
                    ReturnValue = ReturnValue + "<span class ='" + DisableClassName + "'> Prev </span>&nbsp&nbsp&nbsp;";
                if ((PageNumber - 4) > 1)
                    ReturnValue = ReturnValue + "<a href='" + PageUrl.Trim() + "' class='" + ClassName + "'>1</a>&nbsp&nbsp&nbsp;";
                for (int i = PageNumber - 3; i <= PageNumber; i++)
                    if (i >= 1)
                    {
                        if (PageNumber != i)
                        {
                            ReturnValue = ReturnValue + "<a href='" + PageUrl.Trim();
                            if (PageUrl.Contains("?"))
                                ReturnValue = ReturnValue + "&";
                            else
                                ReturnValue = ReturnValue + "?";
                            ReturnValue = ReturnValue + "pn=" + i.ToString() + "' class='" + ClassName + "'>" + i.ToString() + "</a>";

                        }
                        else
                        {
                            ReturnValue = ReturnValue + "<span style= 'font-weight:bold;background-color:#ff0600;color:#fff;padding :5px;border-radius:8px;'></span>+";
                        }
                    }
                for (int i = PageNumber + 1; i <= PageNumber + 4; i++)
                    if (i <= TotalPages)
                    {
                        if (PageNumber != i)
                        {
                            ReturnValue = ReturnValue + "<a href ='" + PageUrl.Trim();
                            if (PageUrl.Contains("?"))
                                ReturnValue = ReturnValue + "&";
                            else
                                ReturnValue = ReturnValue + "?";
                            ReturnValue = ReturnValue + "pn=" + i.ToString() + "' class='" + ClassName + "'>" + i.ToString() + "</a>";

                        }
                        else
                        {
                            ReturnValue = ReturnValue + "<span style='font-weight:bold;'>" + i + "</span>";
                        }
                    }
                if ((PageNumber + 3) < TotalPages)
                {
                    ReturnValue = ReturnValue + ".......&nbsp;<a href='" + PageUrl.Trim();
                    if (PageUrl.Contains("?"))
                        ReturnValue = ReturnValue + "&";
                    else
                        ReturnValue = ReturnValue + "?";
                    ReturnValue = ReturnValue + "pn=" + TotalPages.ToString() + "'class='" + ClassName + "'>" + TotalPages.ToString() + "</a>";
                }
                if (PageNumber < TotalPages)
                {
                    ReturnValue = ReturnValue + "<a href ='" + PageUrl.Trim();
                    if (PageUrl.Contains("?"))
                        ReturnValue = ReturnValue + "&";
                    else
                        ReturnValue = ReturnValue + "?";
                    ReturnValue = ReturnValue + "pn=" + Convert.ToString(PageNumber + 1) + "' class ='" + ClassName + "'>Next</a>";
                }
                else
                {
                    ReturnValue = ReturnValue + "<span class='" + DisableClassName + "'> Next </span>";
                }
            }
            catch (Exception ex)
            {

            }
            return (ReturnValue);
        }
    //    protected void getData()
    //    {
    //        SqlConnection conn = new SqlConnection(db.connetionString);
    //        SqlCommand Cmd = new SqlCommand();
    //        //SqlDataReader sqlReader;
            
    //            Cmd = new SqlCommand("sp_b_pricelist", conn);
    //            Cmd.CommandType = CommandType.StoredProcedure;
    //            Cmd.Parameters.Add("@qtype", SqlDbType.VarChar, 50).Value = "selectList";
    //            Cmd.Parameters.Add("@MSG", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
    //            conn.Open();
    //            //sqlReader = Cmd.ExecuteReader();
    //            //if (sqlReader.HasRows)
    //            //{
    //            //    rptPriceList.DataSource = sqlReader;
    //            //    rptPriceList.DataBind();
    //            //    pnlNoRecord.Visible = false;
    //            //}
    //            //else
    //            //{
    //            //    pnlNoRecord.Visible = true;
    //            //}
    //            DataTable dt = new DataTable();
    //            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
    //            adp.Fill(dt);
    //            PagedDataSource pgitems = new PagedDataSource();
    //            DataView dv = new DataView(dt);
    //            pgitems.DataSource = dv;
    //            pgitems.AllowPaging = true;
    //            pgitems.PageSize = 50;
    //            pgitems.CurrentPageIndex = PageNumber;
    //            if (pgitems.PageCount > 1)
    //            {
    //                rptPaging.Visible = true;
    //                ArrayList pages = new ArrayList();
    //                for (int i = 0; i < pgitems.PageCount; i++)
    //                    pages.Add((i + 1).ToString());
    //                rptPaging.DataSource = pages;
    //                rptPaging.DataBind();
    //            }
    //            else
    //             {

    //              rptPaging.Visible = false;
    //              pnlNoRecord.Visible = true;
    //            }
    //        rptPriceList.DataSource = pgitems;
    //        rptPriceList.DataBind();
    //        conn.Close();

            
    //}
    //public int PageNumber
    //{
    //    get
    //    {
    //        if (ViewState["PageNumber"] != null)
    //            return Convert.ToInt32(ViewState["PageNumber"]);
    //        else
    //            return 0;
    //    }
    //    set
    //    {
    //        ViewState["PageNumber"] = value;
    //    }
    // }

    //protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
    //{
    //    PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
    //    getData();
    //}

   
        
        //catch (Exception ex)
            //{
            //    Response.Write(ex);
            //    pnlerror.Visible = true;
            //    lblError.Text = "Error Occured please Contact Administrator";
            //    noError = false;
            //}
            //finally
            //{
            //    conn.Close();
            //}
        
        

        protected void rptPriceList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                int notifyID = Convert.ToInt32(e.CommandArgument.ToString());
                string rMSG = "";
                rMSG = deleteFn(notifyID);
                if (noError)
                {
                    Response.Redirect("listpricelist.aspx?msg=" + rMSG);
                }
                else
                {
                    pnlerror.Visible = true;
                    lblError.Text = "Error Occured please Contact Administrator";
                    noError = false;
                }
            }
        }
       

        protected string deleteFn(int partlistID)
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            string rStatus = "";
            try
            {
                Cmd = new SqlCommand("sp_b_pricelist", conn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("@partlistID", SqlDbType.Int).Value = partlistID;
                Cmd.Parameters.Add("@qtype", SqlDbType.VarChar, 20).Value = "del";
                Cmd.Parameters.Add("@MSG", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                conn.Open();
                Cmd.ExecuteNonQuery();
                rStatus = Cmd.Parameters["@MSG"].Value.ToString();
                conn.Close();
            }
            catch (Exception ex)
            {
                // Response.Write(ex);
                pnlerror.Visible = true;
                lblError.Text = "Error Occured please Contact Administrator";
                noError = false;
            }
            return rStatus;
        }

        
        protected void btn_submit_Click(object sender, EventArgs e)
        {
            this.isSearch = true;
            this.GetPriceList(1, "search");
        }

        public void bind_product()
        {
            rptPriceList.DataSource = Search();
            rptPriceList.DataBind();
        }
        protected DataTable Search()
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            string value = txt_search.Text;
            string query = string.Empty;
            if (!string.IsNullOrEmpty(value))
                query = "select ROW_NUMBER() OVER (ORDER BY partlistID)[RowNumber],* from Pricelist where  newpartno like '%" + value + "%'or oldpartno like '%" + value + "%'or description like '%" + value + "%'or listprice like '%" + value + "%' or mrp like '%" + value + "%'";
            else
                query = "select ROW_NUMBER() OVER (ORDER BY partlistID)[RowNumber],* from Pricelist";
            SqlDataAdapter sda1 = new SqlDataAdapter(query, conn);
            DataTable dt = new DataTable();
            sda1.Fill(dt);
            return dt;
        }

        protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int iPageIndex = Convert.ToInt32(e.CommandArgument);
            if (this.isSearch)
            {
                this.GetPriceList(iPageIndex, "search");  
            }
            else
            {
                this.GetPriceList(iPageIndex, "selectList");  
            }
            
        }
        protected override void InitializeCulture()
        {
            CultureInfo ci = new CultureInfo("en-IN");
            ci.NumberFormat.CurrencySymbol = "₹";
            Thread.CurrentThread.CurrentCulture = ci;

            base.InitializeCulture();
        }  

       
        

    }


}