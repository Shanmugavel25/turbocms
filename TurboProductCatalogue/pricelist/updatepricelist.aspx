﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="updatepricelist.aspx.cs" Inherits="TurboProductCatalogue.pricelist.updatepricelist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section class="content-header">
        <h1>
            <i class="fa fa-bell "></i>&nbsp;Update Price List
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-10">
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <div class="box  box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="lblHead" runat="server" Text=""></asp:Label>
                            Price List</h3>   
                        <div class="box-tools">
                            <a href="listpricelist.aspx" class="pull-right btn btn-success btn-sm">List Price</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> OldPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox1" ReadOnly="true"  MaxLength="50" CssClass="form-control" placeholder="Enter OldpartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="reqTitle" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox1" runat="server" ErrorMessage='Enter OldpartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>NewPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox2" ReadOnly="true"  MaxLength="50" CssClass="form-control" placeholder="Enter  NewPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox2" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Description <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox3"  MaxLength="50" CssClass="form-control" placeholder="Enter Description " runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox3" runat="server" ErrorMessage='Enter Description'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>List Price <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox4"  MaxLength="50" CssClass="form-control" placeholder="Enter Sales Price" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox4" runat="server" ErrorMessage='Enter sales price'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>MRP <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox5"  MaxLength="50" CssClass="form-control" placeholder="Enter MRP" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter mrp'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        </div>
                    <div class="box-footer">
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="btn btn-success pull-right" Text="Submit" />
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
