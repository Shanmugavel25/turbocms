﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.pricelist
{
    public partial class viewpricelist : System.Web.UI.Page
    {
       public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string Price = MyCrypto.GetDecryptedQueryString(Request.QueryString["partlistID"].ToString());
               // Session["Price"] = MyCrypto.GetDecryptedQueryString(Request.QueryString["partlistID"].ToString());

                SqlConnection con = new SqlConnection(db.connetionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("select partlistID,oldpartno,newpartno,description,listprice,mrp from Pricelist where partlistID='" + Price + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    Label1.Text = sdr["oldpartno"].ToString();
                    Label2.Text = sdr["newpartno"].ToString();
                    Label3.Text = sdr["description"].ToString();
                    Label4.Text = sdr["listprice"].ToString();
                    Label5.Text = sdr["mrp"].ToString();
                }
                con.Close();

            }
        }
    }
}