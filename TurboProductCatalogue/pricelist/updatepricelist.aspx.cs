﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.pricelist
{
    public partial class updatepricelist : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                string Price = MyCrypto.GetDecryptedQueryString(Request.QueryString["partlistID"].ToString());
                //Session["Price"] = MyCrypto.GetDecryptedQueryString(Request.QueryString["partlistID"].ToString());

                SqlConnection con = new SqlConnection(db.connetionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("select partlistID,oldpartno,newpartno,description,listprice,mrp from Pricelist where partlistID='" + Price + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if(sdr.Read())
                {
                    TextBox1.Text = sdr["oldpartno"].ToString();
                    TextBox2.Text= sdr["newpartno"].ToString();
                    TextBox3.Text= sdr["description"].ToString();
                    TextBox4.Text= sdr["listprice"].ToString();
                    TextBox5.Text= sdr["mrp"].ToString();
                }
                con.Close();

            }

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string price = MyCrypto.GetDecryptedQueryString(Request.QueryString["partlistID"].ToString());
            string Date = DateTime.Now.ToString();
            SqlConnection con = new SqlConnection(db.connetionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("update Pricelist SET oldpartno='" + TextBox1.Text + "',newpartno='" + TextBox2.Text + "',description='" + TextBox3.Text + "',listprice='" + TextBox4.Text + "',mrp='" + TextBox5.Text + "',updateddate='" + Date + "' where partlistID='" + price + "'", con);
            cmd.ExecuteNonQuery();
            con.Close();

            Response.Write("<script language=javascript>alert('Updated Record Successfully');window.location=('listpricelist.aspx');</script>");

        }
    }
}