﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.productcatalog
{
    public partial class updateproductlist : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string Product = MyCrypto.GetDecryptedQueryString(Request.QueryString["productlistID"].ToString());
                //Session["Product"] = MyCrypto.GetDecryptedQueryString(Request.QueryString["productlistID"].ToString());

                SqlConnection con = new SqlConnection(db.connetionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select productlistID,tca_oldpartno, tca_newpartno, oem_name, oem_partnumber, segment,sub_segment,tc_model,engine,application, coreassemblykit_oldpartno, coreassemblykit_newpartno, overhaulkit_oldpartno,overhaulkit_newpartno,backplate_oldpartno,backplate_newpartno,secondarykit_oldpartno,secondarykit_newpartno,centralhousing_oldpartno,centralhousing_newpartno,compressorwheel_oldpartno,compressorwheel_newpartno,actuatorassembly_oldpartno,actuatorassembly_newpartno,turbinewheel_oldpartno,turbinewheel_newpartno,gasket_oldpartno,gasket_newpartno,alpha,beta,stroking_pressure,setting_pressure,lift,Status,WhatsNew_status from Productlist where productlistID='" + Product + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    TextBox1.Text = sdr["tca_oldpartno"].ToString();
                    TextBox2.Text = sdr["tca_newpartno"].ToString();
                    TextBox3.Text = sdr["oem_name"].ToString();
                    TextBox4.Text = sdr["oem_partnumber"].ToString();
                    TextBox5.Text = sdr["segment"].ToString();
                    TextBox6.Text = sdr["sub_segment"].ToString();
                    TextBox7.Text = sdr["tc_model"].ToString();
                    TextBox8.Text = sdr["engine"].ToString();
                    TextBox9.Text = sdr["application"].ToString();
                    TextBox10.Text = sdr["coreassemblykit_oldpartno"].ToString();
                    TextBox11.Text = sdr["coreassemblykit_newpartno"].ToString();
                    TextBox12.Text = sdr["overhaulkit_oldpartno"].ToString();
                    TextBox13.Text = sdr["overhaulkit_newpartno"].ToString();
                    TextBox14.Text = sdr["backplate_oldpartno"].ToString();
                    TextBox15.Text = sdr["backplate_newpartno"].ToString();
                    TextBox16.Text = sdr["secondarykit_oldpartno"].ToString();
                    TextBox17.Text = sdr["secondarykit_newpartno"].ToString();
                    TextBox18.Text = sdr["centralhousing_oldpartno"].ToString();
                    TextBox19.Text = sdr["centralhousing_newpartno"].ToString();
                    TextBox20.Text = sdr["compressorwheel_oldpartno"].ToString();
                    TextBox21.Text = sdr["compressorwheel_newpartno"].ToString();
                    TextBox22.Text = sdr["actuatorassembly_oldpartno"].ToString();
                    TextBox23.Text = sdr["actuatorassembly_newpartno"].ToString();
                    TextBox24.Text = sdr["turbinewheel_oldpartno"].ToString();
                    TextBox25.Text = sdr["turbinewheel_newpartno"].ToString();
                    TextBox26.Text = sdr["gasket_oldpartno"].ToString();
                    TextBox27.Text = sdr["gasket_newpartno"].ToString();
                    TextBox28.Text = sdr["alpha"].ToString();
                    TextBox29.Text = sdr["beta"].ToString();
                    TextBox30.Text = sdr["stroking_pressure"].ToString();
                    TextBox31.Text = sdr["setting_pressure"].ToString();
                    TextBox32.Text = sdr["lift"].ToString();
                    DropDownList1.SelectedValue = sdr["Status"].ToString();
                    DropDownList2.SelectedValue = sdr["WhatsNew_status"].ToString();

                }
                con.Close();
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string product = MyCrypto.GetDecryptedQueryString(Request.QueryString["productlistID"].ToString());
            string Date = DateTime.Now.ToString();
            SqlConnection con = new SqlConnection(db.connetionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("update Productlist SET tca_oldpartno='" + TextBox1.Text + "', tca_newpartno='" + TextBox2.Text + "', oem_name='" + TextBox3.Text + "', oem_partnumber='" + TextBox4.Text + "', segment='" + TextBox5.Text + "',sub_segment='" + TextBox6.Text + "',tc_model='" + TextBox7.Text + "',engine='" + TextBox8.Text + "',application='" + TextBox9.Text + "', coreassemblykit_oldpartno='" + TextBox10.Text + "', coreassemblykit_newpartno='" + TextBox11.Text + "', overhaulkit_oldpartno='" + TextBox12.Text + "',overhaulkit_newpartno='" + TextBox13.Text + "',backplate_oldpartno='" + TextBox14.Text + "',backplate_newpartno='" + TextBox15.Text + "',secondarykit_oldpartno='" + TextBox16.Text + "',secondarykit_newpartno='" + TextBox17.Text + "',centralhousing_oldpartno='" + TextBox18.Text + "',centralhousing_newpartno='" + TextBox19.Text + "',compressorwheel_oldpartno='" + TextBox20.Text + "',compressorwheel_newpartno='" + TextBox21.Text + "',actuatorassembly_oldpartno='" + TextBox22.Text + "',actuatorassembly_newpartno='" + TextBox23.Text + "',turbinewheel_oldpartno='" + TextBox24.Text + "',turbinewheel_newpartno='" + TextBox25.Text + "',gasket_oldpartno='" + TextBox26.Text + "',gasket_newpartno='" + TextBox27.Text + "',alpha='" + TextBox28.Text + "',beta='" + TextBox29.Text + "',stroking_pressure='" + TextBox30.Text + "',setting_pressure='" + TextBox31.Text + "',lift='" + TextBox32.Text + "',Status='"+ DropDownList1.SelectedValue +"', WhatsNew_status='"+ DropDownList2.SelectedValue +"',updateddate='" + Date +"' where productlistID='" + product + "'", con);
            cmd.ExecuteNonQuery();
            con.Close();

            Response.Write("<script language=javascript>alert('Updated Record Successfully');window.location=('listproductlist.aspx');</script>");

        }
    }
}