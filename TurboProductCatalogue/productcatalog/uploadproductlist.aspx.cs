﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.productcatalog
{
    public partial class uploadproductlist : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btnUpload_Click(object sender, EventArgs e)
        {
            //if (fileUpload.HasFile)
            //{
            //    string FileName = Path.GetFileName(fileUpload.PostedFile.FileName);
            //    string Extension = Path.GetExtension(fileUpload.PostedFile.FileName);
            //    string FolderPath = ConfigurationManager.AppSettings["FolderPath"];                
            //    string FilePath = Server.MapPath("~/Upload/" + FileName);
            //    fileUpload.SaveAs(FilePath);
            //    Import_To_Grid(FilePath, Extension, ddlInsertMode.SelectedValue);
            //}
            String tca_oldpartno;
            String tca_newpartno;
            String oem_name;
            String oem_partnumber;
            String segment;
            String sub_segment;
            String tc_model;
            String engine;
            String application;
            String coreassemblykit_oldpartno;
            String coreassemblykit_newpartno;
            String overhaulkit_oldpartno;
            String overhaulkit_newpartno;
            String backplate_oldpartno;
            String backplate_newpartno;
            String secondarykit_oldpartno;
            String secondarykit_newpartno;
            String centralhousing_oldpartno;
            String centralhousing_newpartno;
            String compressorwheel_oldpartno;
            String compressorwheel_newpartno;
            String actuatorassembly_oldpartno;
            String actuatorassembly_newpartno;
            String turbinewheel_oldpartno;
            String turbinewheel_newpartno;
            String gasket_oldpartno;
            String gasket_newpartno;
            String alpha;
            String beta;
            String stroking_pressure;
            String setting_pressure;
            String lift;
            String Status;
            String WhatsNew_status;
           

            if (ddlInsertMode.SelectedValue == "Append")
            {

                string path = Path.GetFileName(fileUpload.FileName);

                path = path.Replace(" ", "");
                fileUpload.SaveAs(Server.MapPath("~/ProductUpload/") + path);
                String ExcelPath = Server.MapPath("~/ProductUpload/") + path;
                OleDbConnection mycon = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelPath + ";Extended Properties=Excel 8.0;Persist Security Info=False");
                mycon.Open();
                OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", mycon);
                OleDbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    tca_oldpartno = dr[0].ToString();
                    tca_newpartno = dr[1].ToString();
                    oem_name = dr[2].ToString();
                    oem_partnumber = dr[3].ToString();
                    segment = dr[4].ToString();
                    sub_segment = dr[5].ToString();
                    tc_model = dr[6].ToString();
                    engine = dr[7].ToString();
                    application = dr[8].ToString();
                    coreassemblykit_oldpartno = dr[9].ToString();
                    coreassemblykit_newpartno = dr[10].ToString();
                    overhaulkit_oldpartno = dr[11].ToString();
                    overhaulkit_newpartno = dr[12].ToString();
                    backplate_oldpartno = dr[13].ToString();
                    backplate_newpartno = dr[14].ToString();
                    secondarykit_oldpartno = dr[15].ToString();
                    secondarykit_newpartno = dr[16].ToString();
                    centralhousing_oldpartno = dr[17].ToString();
                    centralhousing_newpartno = dr[18].ToString();
                    compressorwheel_oldpartno = dr[19].ToString();
                    compressorwheel_newpartno = dr[20].ToString();
                    actuatorassembly_oldpartno = dr[21].ToString();
                    actuatorassembly_newpartno = dr[22].ToString();
                    turbinewheel_oldpartno = dr[23].ToString();
                    turbinewheel_newpartno = dr[24].ToString();
                    gasket_oldpartno = dr[25].ToString();
                    gasket_newpartno = dr[26].ToString();
                    alpha = dr[27].ToString();
                    beta = dr[28].ToString();
                    stroking_pressure = dr[29].ToString();
                    setting_pressure = dr[30].ToString();
                    lift = dr[31].ToString();
                    Status = dr[32].ToString();
                    WhatsNew_status = dr[33].ToString();

                    savedata(tca_oldpartno,tca_newpartno,
                 oem_name,
                 oem_partnumber,
                 segment,
                 sub_segment,
                 tc_model,
                 engine,
                 application,
                 coreassemblykit_oldpartno,
                 coreassemblykit_newpartno,
                 overhaulkit_oldpartno,
                 overhaulkit_newpartno,
                 backplate_oldpartno,
                 backplate_newpartno,
                 secondarykit_oldpartno,
                 secondarykit_newpartno,
                 centralhousing_oldpartno,
                 centralhousing_newpartno,
                 compressorwheel_oldpartno,
                 compressorwheel_newpartno,
                 actuatorassembly_oldpartno,
                 actuatorassembly_newpartno,
                 turbinewheel_oldpartno,
                 turbinewheel_newpartno,
                 gasket_oldpartno,
                 gasket_newpartno,
                 alpha,
                 beta,
                 stroking_pressure,
                 setting_pressure,
                 lift, Status, WhatsNew_status);
                }

                Label1.Text = "Data Has Been Saved Successfully";
                BindGridView();
            }
            else if (ddlInsertMode.SelectedValue == "Override")
            {
                SqlConnection con = new SqlConnection(db.connetionString);
                con.Open();
                SqlCommand cmd1 = new SqlCommand("Delete  from Productlist", con);
                cmd1.ExecuteNonQuery();

                string path = Path.GetFileName(fileUpload.FileName);
                path = path.Replace(" ", "");
                fileUpload.SaveAs(Server.MapPath("~/ProductUpload/") + path);
                String ExcelPath = Server.MapPath("~/ProductUpload/") + path;
                OleDbConnection mycon = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelPath + ";Extended Properties=Excel 8.0;Persist Security Info=False");
                mycon.Open();
                OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", mycon);
                OleDbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    tca_oldpartno = dr[0].ToString();
                    tca_newpartno = dr[1].ToString();
                    oem_name = dr[2].ToString();
                    oem_partnumber = dr[3].ToString();
                    segment = dr[4].ToString();
                    sub_segment = dr[5].ToString();
                    tc_model = dr[6].ToString();
                    engine = dr[7].ToString();
                    application = dr[8].ToString();
                    coreassemblykit_oldpartno = dr[9].ToString();
                    coreassemblykit_newpartno = dr[10].ToString();
                    overhaulkit_oldpartno = dr[11].ToString();
                    overhaulkit_newpartno = dr[12].ToString();
                    backplate_oldpartno = dr[13].ToString();
                    backplate_newpartno = dr[14].ToString();
                    secondarykit_oldpartno = dr[15].ToString();
                    secondarykit_newpartno = dr[16].ToString();
                    centralhousing_oldpartno = dr[17].ToString();
                    centralhousing_newpartno = dr[18].ToString();
                    compressorwheel_oldpartno = dr[19].ToString();
                    compressorwheel_newpartno = dr[20].ToString();
                    actuatorassembly_oldpartno = dr[21].ToString();
                    actuatorassembly_newpartno = dr[22].ToString();
                    turbinewheel_oldpartno = dr[23].ToString();
                    turbinewheel_newpartno = dr[24].ToString();
                    gasket_oldpartno = dr[25].ToString();
                    gasket_newpartno = dr[26].ToString();
                    alpha = dr[27].ToString();
                    beta = dr[28].ToString();
                    stroking_pressure = dr[29].ToString();
                    setting_pressure = dr[30].ToString();
                    lift = dr[31].ToString();
                    Status = dr[32].ToString();
                    WhatsNew_status = dr[33].ToString();

                    savedata(tca_oldpartno, tca_newpartno,
                 oem_name,
                 oem_partnumber,
                 segment,
                 sub_segment,
                 tc_model,
                 engine,
                 application,
                 coreassemblykit_oldpartno,
                 coreassemblykit_newpartno,
                 overhaulkit_oldpartno,
                 overhaulkit_newpartno,
                 backplate_oldpartno,
                 backplate_newpartno,
                 secondarykit_oldpartno,
                 secondarykit_newpartno,
                 centralhousing_oldpartno,
                 centralhousing_newpartno,
                 compressorwheel_oldpartno,
                 compressorwheel_newpartno,
                 actuatorassembly_oldpartno,
                 actuatorassembly_newpartno,
                 turbinewheel_oldpartno,
                 turbinewheel_newpartno,
                 gasket_oldpartno,
                 gasket_newpartno,
                 alpha,
                 beta,
                 stroking_pressure,
                 setting_pressure,
                 lift, Status, WhatsNew_status);
                }

                Label1.Text = "Data Has Been Saved Successfully";
                BindGridView();


            }
            else
            {
                Response.Write("<script language='javascript'>alert('Select a uploadedtype')</script>");
            }

        }
        private void BindGridView()
        {
            SqlConnection con1 = new SqlConnection(db.connetionString);
            con1.Open();
            SqlCommand cmd2 = new SqlCommand("Select tca_oldpartno, tca_newpartno, oem_name, oem_partnumber, segment,sub_segment,tc_model,engine,application, coreassemblykit_oldpartno, coreassemblykit_newpartno, overhaulkit_oldpartno,overhaulkit_newpartno,backplate_oldpartno,backplate_newpartno,secondarykit_oldpartno,secondarykit_newpartno,centralhousing_oldpartno,centralhousing_newpartno,compressorwheel_oldpartno,compressorwheel_newpartno,actuatorassembly_oldpartno,actuatorassembly_newpartno,turbinewheel_oldpartno,turbinewheel_newpartno,gasket_oldpartno,gasket_newpartno,alpha,beta,stroking_pressure,setting_pressure,lift,Status,WhatsNew_status from Productlist", con1);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable ds = new DataTable();
            da.Fill(ds);
            GridView1.DataSource = ds;
            GridView1.EmptyDataText = "No Data Found";
            GridView1.DataBind();
            con1.Close();


        }
        private void savedata(String tca_oldpartno1,String tca_newpartno1,String oem_name1,String oem_partnumber1,String segment1,String sub_segment1,String tc_model1,
        String engine1,
        String application1,
        String coreassemblykit_oldpartno1,
        String coreassemblykit_newpartno1,
        String overhaulkit_oldpartno1,
        String overhaulkit_newpartno1,
        String backplate_oldpartno1,
        String backplate_newpartno1,
        String secondarykit_oldpartno1,
        String secondarykit_newpartno1,
        String centralhousing_oldpartno1,
        String centralhousing_newpartno1,
        String compressorwheel_oldpartno1,
        String compressorwheel_newpartno1,
        String actuatorassembly_oldpartno1,
        String actuatorassembly_newpartno1,
        String turbinewheel_oldpartno1,
        String turbinewheel_newpartno1,
        String gasket_oldpartno1,
        String gasket_newpartno1,
        String alpha1,
        String beta1,
        String stroking_pressure1,
        String setting_pressure1,
        String lift1, String Status1, String WhatsNew_status1)
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("insert into Productlist(tca_oldpartno,tca_newpartno,oem_name,oem_partnumber,segment,sub_segment,tc_model,engine,application,coreassemblykit_oldpartno,coreassemblykit_newpartno,overhaulkit_oldpartno,overhaulkit_newpartno,backplate_oldpartno,backplate_newpartno,secondarykit_oldpartno,secondarykit_newpartno,centralhousing_oldpartno,centralhousing_newpartno,compressorwheel_oldpartno,compressorwheel_newpartno,actuatorassembly_oldpartno,actuatorassembly_newpartno,turbinewheel_oldpartno,turbinewheel_newpartno,gasket_oldpartno,gasket_newpartno,alpha,beta,stroking_pressure,setting_pressure,lift,Status,WhatsNew_status)Values('" + tca_oldpartno1 + "','" + tca_newpartno1 + "','" + oem_name1 + "','" + oem_partnumber1 + "','" + segment1 + "','" + sub_segment1 + "','" + tc_model1 + "','" + engine1 + "','" + application1 + "','" + coreassemblykit_oldpartno1 + "','" + coreassemblykit_newpartno1 + "','" + overhaulkit_oldpartno1 + "','" + overhaulkit_newpartno1 + "','" + backplate_oldpartno1 + "','" + backplate_newpartno1 + "','" + secondarykit_oldpartno1 + "','" + secondarykit_newpartno1 + "','" + centralhousing_oldpartno1 + "','" + centralhousing_newpartno1 + "','" + compressorwheel_oldpartno1 + "','" + compressorwheel_newpartno1 + "','" + actuatorassembly_oldpartno1 + "','" + actuatorassembly_newpartno1 + "','" + turbinewheel_oldpartno1 + "','" + turbinewheel_newpartno1 + "','" + gasket_oldpartno1 + "','" + gasket_newpartno1 + "','" + alpha1 + "','" + beta1 + "','" + stroking_pressure1 + "','" + setting_pressure1 + "','" + lift1 + "','" + Status1 + "','" + WhatsNew_status1 + "')", conn);


            cmd.ExecuteNonQuery();
            conn.Close();


        }
       

        protected void btnExcelTemplate_Click(object sender, EventArgs e)
        {
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=productlist_template.xlsx");
            Response.WriteFile(Server.MapPath("~/ProductUpload/productlist_template.xlsx"));
            Response.Flush();
            Response.End();

           
        }
    }
}