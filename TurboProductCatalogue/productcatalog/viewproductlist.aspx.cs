﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.productcatalog
{
    public partial class viewproductlist : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string Product = MyCrypto.GetDecryptedQueryString(Request.QueryString["productlistID"].ToString());
               // Session["Product"] = MyCrypto.GetDecryptedQueryString(Request.QueryString["productlistID"].ToString());

                SqlConnection con = new SqlConnection(db.connetionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select productlistID,tca_oldpartno, tca_newpartno, oem_name, oem_partnumber, segment,sub_segment,tc_model,engine,application, coreassemblykit_oldpartno, coreassemblykit_newpartno, overhaulkit_oldpartno,overhaulkit_newpartno,backplate_oldpartno,backplate_newpartno,secondarykit_oldpartno,secondarykit_newpartno,centralhousing_oldpartno,centralhousing_newpartno,compressorwheel_oldpartno,compressorwheel_newpartno,actuatorassembly_oldpartno,actuatorassembly_newpartno,turbinewheel_oldpartno,turbinewheel_newpartno,gasket_oldpartno,gasket_newpartno,alpha,beta,stroking_pressure,setting_pressure,lift,Status,WhatsNew_status from Productlist where productlistID='" + Product + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    Label1.Text = sdr["tca_oldpartno"].ToString();
                    Label2.Text = sdr["tca_newpartno"].ToString();
                    Label3.Text = sdr["oem_name"].ToString();
                    Label4.Text = sdr["oem_partnumber"].ToString();
                    Label5.Text = sdr["segment"].ToString();
                    Label6.Text = sdr["sub_segment"].ToString();
                    Label7.Text = sdr["tc_model"].ToString();
                    Label8.Text = sdr["engine"].ToString();
                    Label9.Text = sdr["application"].ToString();
                    Label10.Text = sdr["coreassemblykit_oldpartno"].ToString();
                    Label11.Text = sdr["coreassemblykit_newpartno"].ToString();
                    Label12.Text = sdr["overhaulkit_oldpartno"].ToString();
                    Label13.Text = sdr["overhaulkit_newpartno"].ToString();
                    Label14.Text = sdr["backplate_oldpartno"].ToString();
                    Label15.Text = sdr["backplate_newpartno"].ToString();
                    Label16.Text = sdr["secondarykit_oldpartno"].ToString();
                    Label17.Text = sdr["secondarykit_newpartno"].ToString();
                    Label18.Text = sdr["centralhousing_oldpartno"].ToString();
                    Label19.Text = sdr["centralhousing_newpartno"].ToString();
                    Label20.Text = sdr["compressorwheel_oldpartno"].ToString();
                    Label21.Text = sdr["compressorwheel_newpartno"].ToString();
                    Label22.Text = sdr["actuatorassembly_oldpartno"].ToString();
                    Label23.Text = sdr["actuatorassembly_newpartno"].ToString();
                    Label24.Text = sdr["turbinewheel_oldpartno"].ToString();
                    Label25.Text = sdr["turbinewheel_newpartno"].ToString();
                    Label26.Text = sdr["gasket_oldpartno"].ToString();
                    Label27.Text = sdr["gasket_newpartno"].ToString();
                    Label28.Text = sdr["alpha"].ToString();
                    Label29.Text = sdr["beta"].ToString();
                    Label30.Text = sdr["stroking_pressure"].ToString();
                    Label31.Text = sdr["setting_pressure"].ToString();
                    Label32.Text = sdr["lift"].ToString();
                    Label33.Text = sdr["Status"].ToString();
                    Label34.Text = sdr["WhatsNew_status"].ToString();

                }
                con.Close();
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

        }
    }
}