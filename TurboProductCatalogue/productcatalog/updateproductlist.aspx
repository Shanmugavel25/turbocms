﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="updateproductlist.aspx.cs" Inherits="TurboProductCatalogue.productcatalog.updateproductlist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>
            <i class="fa fa-bell "></i>&nbsp;Update Product Catalog
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-10">
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <div class="box  box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="lblHead" runat="server" Text=""></asp:Label>
                            Notification</h3>   
                        <div class="box-tools">
                            <a href="listproductlist.aspx" class="pull-right btn btn-success btn-sm">List Product Catalog</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>TCA OldPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox1"  MaxLength="50"  CssClass="form-control" ReadOnly="true" placeholder="Enter TCA OldpartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="reqTitle" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox1" runat="server" ErrorMessage='Enter OldpartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>TCA NewPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox2"  MaxLength="50"   CssClass="form-control" ReadOnly="true"  placeholder="Enter TCA NewPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox2" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>OEM Name <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox3"  MaxLength="50" CssClass="form-control" placeholder="Enter OEM Name" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox3" runat="server" ErrorMessage='Enter OEM Name'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>OEM PartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox4"  MaxLength="50" CssClass="form-control" placeholder="Enter OEM PartNo" runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox4" runat="server" ErrorMessage='Enter PartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Segment <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox5"  MaxLength="50" CssClass="form-control" placeholder="Enter Segment" runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Sub Segment <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox6"  MaxLength="50" CssClass="form-control" placeholder="Enter SubSegment" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox6" runat="server" ErrorMessage='Enter SubSegment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>TC Model <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox7"  MaxLength="50" CssClass="form-control" placeholder="Enter TC Model" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox7" runat="server" ErrorMessage='Enter Model'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Engine <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox8"  MaxLength="50" CssClass="form-control" placeholder="Enter Engine" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox8" runat="server" ErrorMessage='Enter Engine'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Application <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox9"  MaxLength="50" CssClass="form-control" placeholder="Enter Application" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox9" runat="server" ErrorMessage='Enter Application'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label> Core Assembly Kit OldPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox10"  MaxLength="50" CssClass="form-control" placeholder="Enter OldPartNo " runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox10" runat="server" ErrorMessage='Enter OldPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Core Assembly Kit NewPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox11"  MaxLength="50" CssClass="form-control" placeholder="Enter NewPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox11" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>OverHaul Kit OldPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox12"  MaxLength="50" CssClass="form-control" placeholder="Enter OldPartNo" runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator11" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox12" runat="server" ErrorMessage='Enter OldPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>OverHaul KIt NewPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox13"  MaxLength="50" CssClass="form-control" placeholder="Enter NewPartNo" runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator12" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox13" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Black Plate OldPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox14"  MaxLength="50" CssClass="form-control" placeholder="Enter OldPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator13" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox14" runat="server" ErrorMessage='Enter OldPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Black Plate NewPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox15"  MaxLength="50" CssClass="form-control" placeholder="Enter NewPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator14" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox15" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Secondary Kit OldPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox16"  MaxLength="50" CssClass="form-control" placeholder="Enter OldPartNo" runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator15" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox16" runat="server" ErrorMessage='Enter OldPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Secondary Kit NewPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox17"  MaxLength="50" CssClass="form-control" placeholder="Enter NewPartNo" runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator16" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox17" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Center Housing OldPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox18"  MaxLength="50" CssClass="form-control" placeholder="Enter OldPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator17" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox18" runat="server" ErrorMessage='Enter OldPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Center Housing NewPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox19"  MaxLength="50" CssClass="form-control" placeholder="Enter NewPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator18" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox19" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Compressor Wheel OldPartNO <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox20"  MaxLength="50" CssClass="form-control" placeholder="Enter OldPartNO" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator19" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox20" runat="server" ErrorMessage='Enter OldPartNO'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Compressor Wheel NewPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox21"  MaxLength="50" CssClass="form-control" placeholder="Enter NewPartNo" runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator20" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox21" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Actuator Assembly OldPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox22"  MaxLength="50" CssClass="form-control" placeholder="Enter OldPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator21" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox22" runat="server" ErrorMessage='Enter OldPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Actuator Assembly NewPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox23"  MaxLength="50" CssClass="form-control" placeholder="Enter NewPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator22" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox23" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Turbine Wheel Assembly OldPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox24"  MaxLength="50" CssClass="form-control" placeholder="Enter OldPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator23" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox24" runat="server" ErrorMessage='Enter Title'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Turbine Wheel Assembly NewPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox25"  MaxLength="50" CssClass="form-control" placeholder="Enter NewPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator24" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox25" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Gasket OldPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox26"  MaxLength="50" CssClass="form-control" placeholder="Enter OldPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator25" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox26" runat="server" ErrorMessage='Enter OldPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Gasket NewPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox27"  MaxLength="50" CssClass="form-control" placeholder="Enter NewPartNo" runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator26" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox27" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Alpha <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox28"  MaxLength="50" CssClass="form-control" placeholder="Enter Alpha " runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator27" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox28" runat="server" ErrorMessage='Enter Alpha'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Beta <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox29"  MaxLength="50" CssClass="form-control" placeholder="Enter Beta" runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator28" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox29" runat="server" ErrorMessage='Enter Beta'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Storking Pressure <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox30"  MaxLength="50" CssClass="form-control" placeholder="Enter Pressure" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator29" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox30" runat="server" ErrorMessage='Enter Pressure'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Setting Pressure <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox31"  MaxLength="50" CssClass="form-control" placeholder="Enter Pressure" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator30" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox31" runat="server" ErrorMessage='Enter Pressure'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Lift <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox32"  MaxLength="50" CssClass="form-control" placeholder="Enter Lift" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator31" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox32" runat="server" ErrorMessage='Enter Lift'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status<span class="text-danger">*</span></label>
                                <%--<asp:TextBox ID="TextBox33"  MaxLength="50" CssClass="form-control" placeholder="Enter Staus" runat="server"></asp:TextBox>
                                --%><asp:DropDownList ID="DropDownList1" MaxLength="50" CssClass="form-control" runat="server">
                                    <asp:ListItem Value="Active"> Active</asp:ListItem>
                                    <asp:ListItem Value="Deactive">Deactive</asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator31" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox32" runat="server" ErrorMessage='Enter Lift'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> New Status <span class="text-danger">*</span></label>
                                <%--<asp:TextBox ID="TextBox34"  MaxLength="50" CssClass="form-control" placeholder="Enter New Status" runat="server"></asp:TextBox>
                                --%><asp:DropDownList ID="DropDownList2" MaxLength="50" CssClass="form-control" runat="server">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator31" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox32" runat="server" ErrorMessage='Enter Lift'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        

                        

                    </div>
                    <div class="box-footer">
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="btn btn-success pull-right" Text="Submit" />
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
