﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="listproductlist.aspx.cs" Inherits="TurboProductCatalogue.productcatalog.listproductlist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
     
.pagination {
  display: inline-block;
}

.pagination a {
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
}

.pagination a.active {
  background-color: #4CAF50;
  color: black;
  border-radius: 5px;
}

.pagination a:hover:not(.active) {
  background-color: #ddd;
  border-radius: 5px;
}
  </style> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>
            <i class="fa fa-pinterest-square "></i>&nbsp;Product List
        </h1>
    </section>   
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pnlMSG" runat="server" Visible="false">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblMSG" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>

                <div class="well">
                    <div class="row">
                        <div class="col-md-4 ">
                            <asp:TextBox runat="server" ID="txt_search" placeholder="Type Something ..." CssClass="form-control"></asp:TextBox>
                            </div>
                            <%--<asp:RequiredFieldValidator ID="req" runat="server" ErrorMessage="Type Something ..." ControlToValidate="txt_search" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>--%>
                        <div class="col-md-4 ">
                            <asp:Button class="btn btn-primary" ID="Button1" ValidationGroup="btnSearch" Text="Search" runat="server" OnClick="btn_submit_Click" />


                        </div>
               
                </div>
                    </div>

                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="">List of Product</h3>
                        <div class="box-tools">
                            <a href="uploadproductlist.aspx" class="btn btn-success btn-sm">Upload Product Catalog</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                           
                            <asp:Repeater ID="rptProductList" runat="server" OnItemCommand="rptProductList_ItemCommand" >
                                <HeaderTemplate>
                                    <table class="table table-hover table-bordered">
                                        <tr class="success">
                                            <th class="text-center" style="width: 5%">S.No</th>
                                            <th class="text-center" style="width: 10%">TCA Old Part No</th>
                                            <th class="text-center" style="width: 10%"> TCA New Part No</th>
                                            <th class="text-center" style="width: 20%">OEM Name</th>
                                            <th class="text-center" style="width: 15%">OEM Part No</th>
                                            <th class="text-center" style="width: 10%">Segment</th>
                                            <th class="text-center" style="width: 15%">Sub Segment</th>
                                          <%--  <th class="text-center" style="width: 15%">TC MOdel</th>--%>
                                            <th class="text-center" style="width: 40%"> Action</th>
                                            <%--<th class="text-center" style="width: 25%">Engine</th>
                                            <th class="text-center" style="width: 25%">Application</th>
                                            <th class="text-center" style="width: 25%">Core Assembly Kit OldPartNo</th>
                                            <th class="text-center" style="width: 25%">Core Assembly Kit NewPartNo</th>
                                            <th class="text-center" style="width: 25%">Over Haul Kit OldPartNo</th>
                                            <th class="text-center" style="width: 25%">Over Haul Kit NewPartNo</th>
                                            <th class="text-center" style="width: 25%">BackPlate OldpartNo</th>
                                            <th class="text-center" style="width: 25%">BackPlate NewPartNo</th>
                                            <th class="text-center" style="width: 25%">SecondaryKit OldPartNo</th>
                                            <th class="text-center" style="width: 25%">SecondaryKit NewpartNo</th>
                                            <th class="text-center" style="width: 25%">Central Housing OldPartNO</th>
                                            <th class="text-center" style="width: 25%">Central Housing NewPartNo</th>
                                            <th class="text-center" style="width: 25%">Compressor Wheel OldPartNo</th>
                                            <th class="text-center" style="width: 25%">Compressor Wheel NewPartNo</th>
                                            <th class="text-center" style="width: 25%">Actuator Assembly OldPartNo</th>
                                            <th class="text-center" style="width: 25%">Actuator Assembly NewPartNo </th>
                                            <th class="text-center" style="width: 25%">Turbin Wheel OldPartNo</th>
                                            <th class="text-center" style="width: 25%">Turbin Wheel NewPartNo</th>
                                            <th class="text-center" style="width: 25%">Gasket OldPartNo</th>
                                            <th class="text-center" style="width: 25%">Gasket NewPartNo</th>
                                            <th class="text-center" style="width: 25%">Alpha</th>
                                            <th class="text-center" style="width: 25%">Beta</th>
                                             <th class="text-center" style="width: 25%">Storking Pressure</th>
                                             <th class="text-center" style="width: 25%">Setting Pressure</th>
                                             <th class="text-center" style="width: 25%">Lift</th>
                                            <th class="text-center" style="width: 25%">Status</th>
                                          <th class="text-center" style="width: 25%">WhatsNew_status</th>--%>

                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="text-center"><%# DataBinder.Eval(Container.DataItem,"slNo")%></td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"tca_oldpartno")%>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"tca_newpartno")%>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"oem_name")%>
                                        </td>
                                        <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"oem_partnumber")%>
                                        </td>
                                        <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"segment")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"sub_segment")%>
                                        </td>
                                       <%--  <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"tc_model")%>
                                        </td>--%>
                                         <%-- <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"engine")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"application")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"coreassemblykit_oldpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"coreassemblykit_newpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"overhaulkit_oldpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"overhaulkit_newpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"backplate_oldpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"backplate_newpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"secondarykit_oldpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"secondarykit_newpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"centralhousing_oldpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"centralhousing_newpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"compressorwheel_oldpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"compressorwheel_newpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"actuatorassembly_oldpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"actuatorassembly_newpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"turbinewheel_oldpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"turbinewheel_newpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"gasket_oldpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"gasket_newpartno")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"alpha")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"beta")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"stroking_pressure")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"setting_pressure")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"lift")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"Status")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"WhatsNew_status")%>
                                        </td>--%>
                                        

                                        <td class="text-center">
                                            <a href="updateproductlist.aspx?mode=edit&productlistID=<%# TurboProductCatalogue.MyCrypto.GetEncryptedQueryString (DataBinder.Eval(Container.DataItem,"productlistID").ToString())%>" title="Edit ProductList" data-toggle="tooltip" data-placement="top" class="btn btn-default btn-xs btn-warning "><i class="fa fa-edit iconColor"></i></a>
                                            <a href="viewproductlist.aspx?mode=edit&productlistID=<%# TurboProductCatalogue.MyCrypto.GetEncryptedQueryString ( DataBinder.Eval(Container.DataItem,"productlistID").ToString())%>" title="View ProductList" data-toggle="tooltip" data-placement="top" class="btn btn-primary btn-xs btn-primary "><i class="fa fa-eye iconColor"></i></a>
                                            <asp:LinkButton ID="lnkDelete" ToolTip="Delete" CommandName="delete" CommandArgument='<%# Eval("productlistID") %>'  class="btn btn-default btn-xs btn-danger" data-toggle="tooltip" data-placement="top" runat="server" Text='Delete User' OnClientClick='javascript:return confirm("Are you sure you want to delete?")'><i class="fa fa-trash iconColor"></i></asp:LinkButton>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>

                            </asp:Repeater>
                            <br />  
        <div class="pagination" >

                                    <asp:Repeater ID="Repeater2" runat="server" OnItemCommand="Repeater2_ItemCommand">  
                <ItemTemplate>  
                    <asp:LinkButton ID="lnkPage" 
                         onmouseover="this.style.background='Green';" onmouseout="this.style.background='white';" 
                        Style="padding: 8px; margin: 2px; background: lightgray; border: solid 1px #666; color: black; font-weight: bold"  
                        CommandName="Page" CommandArgument="<%# Container.DataItem %>" runat="server" Font-Bold="True"><%# Container.DataItem %>  
                    </asp:LinkButton>  
                </ItemTemplate>  
            </asp:Repeater>  
        </div>
                            <asp:Panel ID="pnlNoRecord" Visible="false" runat="server">
                                <h3 class="text-danger text-bold text-center">No Record Found</h3>
                            </asp:Panel>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </section>
</asp:Content>
