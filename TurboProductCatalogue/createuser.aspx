﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Masters/Site.Master" CodeBehind="createuser.aspx.cs" Inherits="TurboProductCatalogue.createuser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="content-header">
        <h1>
            <i class="fa fa-users "></i>&nbsp;Create User
        </h1>
    </section>   
    <section class="content">
      <div class="row">
        <div class="col-md-6">
            <asp:Panel ID="pnlError" runat="server" Visible="false">
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
              </div>
            </asp:Panel>
           <div class="box  box-primary">
               <div class="box-header with-border">
                <h3 class="box-title">
                    Create New User</h3>
                </div>
               <div class="box-body">
                <div class="form-group" style=" padding-bottom:25px;">
                  <label class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                    <asp:TextBox ID="txtUserName" type="text" maxlength="50" CssClass="form-control" placeholder="Enter Your Name" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="ReqUserName" ControlToValidate="txtUserName" ForeColor="red" runat="server" ErrorMessage="Enter Your Name"></asp:RequiredFieldValidator>
                  </div>
                </div>
                <div class="form-group" style=" padding-bottom:25px;">
                  <label for="name" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                      <asp:TextBox ID="txtUserEmail" cssclass="form-control" placeholder="Enter Email Id" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="reqEmail" ControlToValidate="txtUserEmail" runat="server" ForeColor="red" ErrorMessage="Enter Your EmailID"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regEmail"  runat="server" ErrorMessage="Enter Your Mail In Correct Format" ControlToValidate="txtUserEmail" ForeColor="red" SetFocusOnError="True"
                     ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                  </div>
                </div>
                
                 <div class="form-group" style=" padding-bottom:25px;">
                  <label for="name" class="col-sm-2 control-label">MobileNo</label>
                  <div class="col-sm-10">
                    <asp:TextBox ID="txtUserMobileNo" type="text" maxlength="50" CssClass="form-control" placeholder="Enter Mobile No"  runat="server" ></asp:TextBox>
                  </div>
                </div>&nbsp;
                
                
             </div>
              <!-- /.box-body -->
              <div class="box-footer">
                  <asp:Button ID="btnSubmit" runat="server" cssclass="btn btn-primary pull-right" Text="Submit" />
              </div>
              <!-- /.box-footer -->
        </div>
     </div>
            </div>
    </section>
</asp:Content>