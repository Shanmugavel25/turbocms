﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Masters/Site.Master" CodeBehind="changepwd.aspx.cs" Inherits="TurboProductCatalogue.changepwd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="content-header">
        <h1>
            <i class="fa fa-key"></i>&nbsp;Change Password
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box  box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Change Password</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group" style="padding-bottom: 25px;">
                            <asp:Label ID="lblOldPassword" CssClass="col-sm-4 control-label text-bold" runat="server" Text="">Old Password<span class="text-red"> *</span></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtOldPwd" runat="server" minlength="8" MaxLength="20" TextMode="Password" CssClass="form-control" required="required" placeholder="Enter Old Password" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  Display="Dynamic" ControlToValidate="txtOldPwd"  ForeColor="Red" errormessage="Password must be 8 characters and have both letters and numbers." validationexpression="(?=.{8,})[a-zA-Z]+[^a-zA-Z]+|[^a-zA-Z]+[a-zA-Z]+" ></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group" style="padding-bottom: 25px;">
                            <asp:Label ID="lblNewPassword" CssClass="col-sm-4 control-label text-bold" runat="server" Text="">New Password<span class="text-red"> *</span></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtNewPwd" runat="server" minlength="8" placeholder="Enter New Password" MaxLength="20" CssClass="form-control" required="required" TextMode="Password" />
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"  Display="Dynamic" ControlToValidate="txtNewPwd"  ForeColor="Red" errormessage="Password must be 8 characters and have both letters and numbers." validationexpression="(?=.{8,})[a-zA-Z]+[^a-zA-Z]+|[^a-zA-Z]+[a-zA-Z]+" ></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group" style="padding-bottom: 25px;">
                            <asp:Label ID="lblConfirmPassword" CssClass="col-sm-4 control-label text-bold" runat="server" Text="">Confirm Password<span class="text-red"> *</span></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="confirmPasswordTextBox" runat="server" placeholder="Enter Confirm Password" minlength="8" MaxLength="20" CssClass="form-control" required="required" TextMode="Password" />
                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        &nbsp;
       <div class="box-footer">
           <b>
               <asp:CompareValidator ID="comparePasswords" runat="server" ControlToCompare="txtNewPwd" ControlToValidate="confirmPasswordTextBox" ErrorMessage="Your passwords do not match up!" Display="Dynamic" CssClass="text-bold text-danger" />
               <asp:RequiredFieldValidator ID="confirmPasswordReq" runat="server" ControlToValidate="confirmPasswordTextBox" ErrorMessage="" SetFocusOnError="True" Display="Dynamic" CssClass="text-bold text-danger" />
               <asp:RequiredFieldValidator ID="passwordReq" runat="server" ControlToValidate="txtNewPwd" ErrorMessage="Password is required!" SetFocusOnError="True" Display="Dynamic" CssClass="text-bold text-danger" />
           </b>
           <asp:Label ID="lblMsg" runat="server" Text="" CssClass="text-bold text-danger"></asp:Label>
           <asp:Button ID="btnSubmit" CssClass="btn btn-primary pull-right" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
       </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
