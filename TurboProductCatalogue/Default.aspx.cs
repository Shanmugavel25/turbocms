﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //sp_configure 'show advanced options', 1;  
            //RECONFIGURE;
            //GO 
            //sp_configure 'Ad Hoc Distributed Queries', 1;  
            //RECONFIGURE;  
            //GO 
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            loginFn();
        }

        protected void loginFn()
        {
            common db = new common();
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            SqlDataReader sqlReader;

            try
            {
                Cmd = new SqlCommand("sp_b_userMaster", conn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("@userName", SqlDbType.VarChar, 50).Value = txtUserName.Text.Trim();
                Cmd.Parameters.Add("@userPassword", SqlDbType.VarChar, 20).Value = txtUserPassword.Text.Trim();
                Cmd.Parameters.Add("@qtype", SqlDbType.VarChar, 20).Value = "selectLogin";
                conn.Open();
                sqlReader = Cmd.ExecuteReader();
                if (sqlReader.HasRows)
                {
                    sqlReader.Read();
                    Session["userID"] = sqlReader["userID"];
                    Session["userName"] = sqlReader["userName"];
                    Response.Redirect("~/dashboard.aspx");
                }
                else
                {
                    lblText.Text = "Sorry Invalid User Name or Password";
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
