﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.Analysis
{
    public partial class TodayReport : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getData();
            }
        }
        protected void getData()
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            
            //SqlDataReader sqlReader;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select Segment,Insertdate,Count from LogNew", conn);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                rptreport.DataSource = ds;
                rptreport.DataBind();

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                conn.Close();
            }
        }

        protected void rptreport_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }
        
    }
}