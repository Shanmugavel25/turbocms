﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="TodayReport.aspx.cs" Inherits="TurboProductCatalogue.Analysis.TodayReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <section class="content-header">
        <h1>
            <i class="fa fa-area-chart"></i>&nbsp;Today Report
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pnlMSG" runat="server" Visible="false">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblMSG" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>

                

                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of Today report</h3>
                        <div class="box-tools">
                            <a href="viewreport.aspx" class="btn btn-success btn-sm"> View Report</a>
                        </div>
                        
                </div>
                    <div class="box-body">
                        <div class="table-responsive">

                            <asp:Repeater ID="rptreport" runat="server" OnItemCommand="rptreport_ItemCommand" >
                                <HeaderTemplate>
                                    <table class="table table-hover table-bordered">
                                        <tr class="success">
                                            <th class="text-center" style="width: 5%">S.No</th>
                                            <th class="text-center" style="width: 30%">Segment</th>
                                            <th class="text-center" style="width: 30%">Date</th>
                                            <th class="text-center" style="width: 10%">Count</th>
                                            <th class="text-center" style="width: 15%">Action</th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="text-center"><%# DataBinder.Eval(Container.DataItem,"slNo")%></td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"Segment")%>
                                        </td>
                                        <td class="text-center">
                                            <%#(DataBinder.Eval(Container.DataItem,"Date"))%>
                                        </td>
                                        <td class="text-center">
                                            <%#(DataBinder.Eval(Container.DataItem,"Count"))%>
                                        </td>
                                        <td class="text-center">
                                             <a href="viewreport.aspx?mode=edit&Id=<%# TurboProductCatalogue.MyCrypto.GetEncryptedQueryString ( DataBinder.Eval(Container.DataItem,"Id").ToString())%>" title="View" data-toggle="tooltip" data-placement="top" class="btn btn-primary btn-xs btn-primary "><i class="fa fa-eye iconColor"></i></a>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>

                            </asp:Repeater>

                            
                            <asp:Panel ID="pnlNoRecord" Visible="false" runat="server">
                                <h3 class="text-danger text-bold text-center">No Record Found</h3>
                            </asp:Panel>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </section>
</asp:Content>
