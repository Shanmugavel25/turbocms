﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue
{
    public partial class dashboard : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getData();
                BindData();
                BindTable();
                //BindValue();
            }

            //SqlConnection con = new SqlConnection(db.connetionString);
            //con.Open();
            //DataTable dt = new DataTable();
            //SqlCommand cmd = new SqlCommand("select approvestatus as status,count(userID) as Total from RegisteredUsers Group by approvestatus", con);
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //da.Fill(dt);
            //con.Close();  //Logistic Algm

            //string[] x = new string[dt.Rows.Count];
            //int[] y = new int[dt.Rows.Count];
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    x[i] = dt.Rows[i][0].ToString();
            //    y[i] = Convert.ToInt32(dt.Rows[i][1]);
            //}

            //Chart1.Series[0].Points.DataBindXY(x, y);
            //Chart1.Series[0].ChartType = SeriesChartType.Pie;
            //Chart1.Series["Series1"].Label = "#PERCENT{P2}";
            //Chart1.Series["Series1"].LegendText = "#VALX";
            //Chart1.Legends[0].LegendStyle = LegendStyle.Column;
            //Chart1.Legends[0].Docking = Docking.Right;
            //Chart1.Legends[0].Alignment = System.Drawing.StringAlignment.Center;

            //Chart1.ChartAreas[0].AxisX.Title = "Registered Details";
            //Chart1.ChartAreas[0].AxisY.Title = "User Status";
            //Chart1.ChartAreas["ChartArea1"].AxisX.IsLabelAutoFit = true;

            //Chart1.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            //Chart1.ChartAreas["ChartArea1"].AxisY.IsLabelAutoFit = true;

            //Chart1.ChartAreas["ChartArea1"].AxisY.Interval = 1;
            //Chart1.ChartAreas[0].AxisX.LabelStyle.Format = "Itemname";
        }
        protected void getData()
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand("select count(*) from RegisteredUsers", conn);
            Int32 count=(Int32)Cmd.ExecuteScalar();
          
            conn.Close();

            label1.Text = count.ToString();
        }
        protected void BindData()
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand("select count(*) from RegisteredUsers where approvestatus='Approved'", conn);
            Int32 count = (Int32)Cmd.ExecuteScalar();

            conn.Close();

            label2.Text = count.ToString();
        }
        protected void BindTable()
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand("select count(*) from RegisteredUsers where approvestatus='Reject'", conn);
            Int32 count = (Int32)Cmd.ExecuteScalar();

            conn.Close();

            label3.Text = count.ToString();
        }
        //protected void BindValue()
        //{
        //    SqlConnection conn = new SqlConnection(db.connetionString);
        //    SqlCommand Cmd = new SqlCommand();
        //    conn.Open();
        //    Cmd = new SqlCommand("select count(*) from RegisteredUsers where approvestatus='Pending'", conn);
        //    Int32 count = (Int32)Cmd.ExecuteScalar();

        //    conn.Close();

        //    label4.Text = count.ToString();
        //}
       

        
    }
}