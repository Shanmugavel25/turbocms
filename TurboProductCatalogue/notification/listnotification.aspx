﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Masters/Site.Master" CodeBehind="listnotification.aspx.cs" Inherits="TurboProductCatalogue.notification.listnotification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="content-header">
        <h1>
            <i class="fa fa-bell"></i>&nbsp;Notifications
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pnlMSG" runat="server" Visible="false">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblMSG" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>


                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of Notifications</h3>
                        <div class="box-tools">
                            <a href="addEditNotification.aspx?mode=add" class="btn btn-success btn-sm">Create Notification</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <asp:Repeater ID="rptNotification" runat="server" OnItemCommand="rptNotification_ItemCommand" >
                                <HeaderTemplate>
                                    <table class="table table-hover table-bordered">
                                        <tr class="success">
                                            <th class="text-center" style="width: 5%">#</th>
                                            <th class="text-center" style="width: 20%">Name</th>
                                            <th class="text-center" style="width: 40%">Description</th>
                                            <th class="text-center" style="width: 10%">Status</th>
                                            <th class="text-center" style="width: 10%">Priority</th>
                                            <th class="text-center" style="width: 15%">Action</th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="text-center"><%# DataBinder.Eval(Container.DataItem,"slNo")%></td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"title")%>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"description")%>
                                        </td>
                                        <td class="text-center">
                                            <%# db.IsActive(DataBinder.Eval(Container.DataItem,"status").ToString())%>
                                        </td>
                                        <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"priority")%>
                                        </td>
                                        <td class="text-center">
                                            <a href="addEditNotification.aspx?mode=edit&notifyID=<%# DataBinder.Eval(Container.DataItem,"NotificationID")%>" title="Edit" data-toggle="tooltip" data-placement="top" class="btn btn-default btn-xs btn-warning "><i class="fa fa-edit iconColor"></i></a>
                                            <asp:LinkButton ID="lnkDelete" ToolTip="Delete" CommandName="delete" CommandArgument='<%# Eval("NotificationID") %>'  class="btn btn-default btn-xs btn-danger" data-toggle="tooltip" data-placement="top" runat="server" Text='Delete User' OnClientClick='javascript:return confirm("Are you sure you want to delete?")'><i class="fa fa-trash iconColor"></i></asp:LinkButton>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>

                            </asp:Repeater>
                            <asp:Panel ID="pnlNoRecord" Visible="false" runat="server">
                                <h3 class="text-danger text-bold text-center">No Record Found</h3>
                            </asp:Panel>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </section>

</asp:Content>
