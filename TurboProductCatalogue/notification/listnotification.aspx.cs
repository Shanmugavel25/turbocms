﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.notification
{
    public partial class listnotification : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {
            string strMSG;
            strMSG = Request.QueryString["msg"];
            if (!string.IsNullOrEmpty(strMSG))
            {
                pnlMSG.Visible = true;
                lblMSG.Text = strMSG;
            }
            if (!IsPostBack)
            {
                getData();
            }
        }

        protected void getData()
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            SqlDataReader sqlReader;
            try
            {
                Cmd = new SqlCommand("sp_b_notification", conn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("@qtype", SqlDbType.VarChar, 50).Value = "selectList";
                Cmd.Parameters.Add("@MSG", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                conn.Open();
                sqlReader = Cmd.ExecuteReader();
                if (sqlReader.HasRows)
                {
                    rptNotification.DataSource = sqlReader;
                    rptNotification.DataBind();
                    pnlNoRecord.Visible = false;
                }
                else
                {
                    pnlNoRecord.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
                pnlerror.Visible = true;
                lblError.Text = "Error Occured please Contact Administrator";
                noError = false;
            }
            finally
            {
                conn.Close();
            }
        }

        protected void rptNotification_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                int notifyID = Convert.ToInt32(e.CommandArgument.ToString());
                string rMSG = "";
                rMSG = deleteFn(notifyID);
                if (noError)
                {
                    Response.Redirect("listnotification.aspx?msg=" + rMSG);
                }
                else
                {
                    pnlerror.Visible = true;
                    lblError.Text = "Error Occured please Contact Administrator";
                    noError = false;
                }
            }
        }

        protected string deleteFn(int NotificationID)
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            string rStatus = "";
            try
            {
                Cmd = new SqlCommand("sp_b_notification", conn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("@NotificationID", SqlDbType.Int).Value = NotificationID;
                Cmd.Parameters.Add("@qtype", SqlDbType.VarChar, 20).Value = "del";
                Cmd.Parameters.Add("@MSG", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                conn.Open();
                Cmd.ExecuteNonQuery();
                rStatus = Cmd.Parameters["@MSG"].Value.ToString();
                conn.Close();
            }
            catch (Exception ex)
            {
                // Response.Write(ex);
                pnlerror.Visible = true;
                lblError.Text = "Error Occured please Contact Administrator";
                noError = false;
            }
            return rStatus;
        }



    }
}