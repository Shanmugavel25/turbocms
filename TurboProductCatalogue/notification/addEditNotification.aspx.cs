﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.notification
{
    public partial class addEditNotification : System.Web.UI.Page
    {
        public common db = new common();
        public int notifyID = 0;
        public string mode = "";
        public bool noError = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            mode = Request.QueryString["mode"];
            notifyID = Convert.ToInt32(Request.QueryString["notifyID"]);
            if (!IsPostBack)
            {
                if (mode == "add")
                {
                    lblHead.Text = "Create New";
                }
                else if (mode == "edit")
                {   
                    lblHead.Text = "Edit";
                    getDataByID(notifyID);
                }
            }
        }

        protected void getDataByID(int notifyID)
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            SqlDataReader sqlReader;

            try
            {
                Cmd = new SqlCommand("sp_b_notification", conn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("@NotificationID", SqlDbType.Int).Value = notifyID;
                Cmd.Parameters.Add("@qtype", SqlDbType.VarChar, 20).Value = "selectByID";
                conn.Open();
                sqlReader = Cmd.ExecuteReader();
                if (sqlReader.HasRows)
                {
                    sqlReader.Read();
                    txtTitle.Text = sqlReader["title"].ToString();
                    txtDesc.Text = sqlReader["description"].ToString();
                    ddlStatus.SelectedValue = sqlReader["status"].ToString();
                    txtPriority.Text = sqlReader["priority"].ToString();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
                pnlerror.Visible = true;
                lblError.Text = "Error Occured, Please Contact Administrator";
                noError = false;
            }
            finally
            {
                conn.Close();
            }
        }

        protected string createUpNotication(string qtype)
        {
            string rMSG = "";
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            try
            {
                Cmd = new SqlCommand("sp_b_notification", conn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("@NotificationID", SqlDbType.Int).Value = notifyID;
                Cmd.Parameters.Add("@title", SqlDbType.VarChar, 150).Value = txtTitle.Text.Trim();
                Cmd.Parameters.Add("@description", SqlDbType.VarChar, 500).Value = txtDesc.Text.Trim();
                Cmd.Parameters.Add("@status", SqlDbType.VarChar, 10).Value = ddlStatus.SelectedValue;
                Cmd.Parameters.Add("@priority", SqlDbType.Int).Value = txtPriority.Text.Trim();
                Cmd.Parameters.Add("@updatedatetime", SqlDbType.DateTime).Value = DateTime.Now;
                Cmd.Parameters.Add("@qtype", SqlDbType.VarChar, 50).Value = qtype;
                Cmd.Parameters.Add("@MSG", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                conn.Open();
                Cmd.ExecuteNonQuery();                
                rMSG = Cmd.Parameters["@MSG"].Value.ToString();
                // Response.Write(rMSG)
                conn.Close();
            }
            catch (Exception ex)
            {
                Response.Write(ex); ;
                pnlerror.Visible = true;
                lblError.Text = "Error Occured, Please Contact Administrator";
                noError = false;
            }
            return rMSG;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string msg = "";

            if (mode == "add")
                msg = createUpNotication("ins");
            else if (mode == "edit")
                msg = createUpNotication("update");

            if (noError)
            {
                Response.Redirect("listnotification.aspx?msg=" + msg );
            }
            else
            {
                pnlerror.Visible = true;
                lblError.Text = "Error Occured please Contact Administrator";
            }
        }

    }
}