﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue
{
    public partial class changepwd : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();

        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(db.connetionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("select userPassword from userMaster where userID='" + Session["userID"].ToString() + "'", con);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                Session["Password"] = sdr["userPassword"].ToString();
            }
            con.Close();

           

            
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            string OldPass = Session["Password"].ToString();
            string NewPass = txtNewPwd.Text;
            string ConfirmPass = confirmPasswordTextBox.Text;

            if(OldPass==txtOldPwd.Text)
            {
                lblOldPassword.Text = "Old Password Match";
                
                if (NewPass == ConfirmPass)
                {
                    Label1.Text = "Password Match";

                    SqlConnection con1 = new SqlConnection(db.connetionString);
                    con1.Open();
                    SqlCommand cmd1 = new SqlCommand("Update userMaster SET userPassword='" + NewPass + "' where userID= '" + Session["userID"].ToString() + "'", con1);
                    cmd1.ExecuteNonQuery();
                    con1.Close();

                    Response.Write("<script language=javascript>alert('Password Changed Sucessfully');window.location=('dashboard.aspx');</script>");

                }
                else
                {
                    Label1.Text = "Password Mismatch";
                }
            }
            else
            {
                lblOldPassword.Text="Old Password Mismatch";

            }
           

            
        }
    }
}