﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.RemanProduct
{
    public partial class updateremanproductlist : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string UpdateProduct = MyCrypto.GetDecryptedQueryString(Request.QueryString["remanID"].ToString());
                //Session["UpdateProduct"] = MyCrypto.GetDecryptedQueryString(Request.QueryString["remanID"].ToString());

                SqlConnection con = new SqlConnection(db.connetionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select remanID,tca_oldpartno, tca_newpartno, oem_name, oem_partnumber, segment,sub_segment,tc_model,engine,application,alpha,beta,stroking_pressure,setting_pressure,lift,Status,WhatsNew_status from remanproduct where remanID='" + UpdateProduct + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    TextBox1.Text = sdr["tca_oldpartno"].ToString();
                    TextBox2.Text = sdr["tca_newpartno"].ToString();
                    TextBox3.Text = sdr["oem_name"].ToString();
                    TextBox4.Text = sdr["oem_partnumber"].ToString();
                    TextBox5.Text = sdr["segment"].ToString();
                    TextBox6.Text = sdr["sub_segment"].ToString();
                    TextBox7.Text = sdr["tc_model"].ToString();
                    TextBox8.Text = sdr["engine"].ToString();
                    TextBox9.Text = sdr["application"].ToString();
                    TextBox10.Text = sdr["alpha"].ToString();
                    TextBox11.Text = sdr["beta"].ToString();
                    TextBox12.Text = sdr["stroking_pressure"].ToString();
                    TextBox13.Text = sdr["setting_pressure"].ToString();
                    TextBox14.Text = sdr["lift"].ToString();
                    DropDownList1.SelectedValue = sdr["Status"].ToString();
                    DropDownList2.SelectedValue = sdr["WhatsNew_status"].ToString();

                }
                con.Close();
            }
        }
            protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string updateProduct = MyCrypto.GetDecryptedQueryString(Request.QueryString["remanID"].ToString());

            string Date = DateTime.Now.ToString();
            SqlConnection con = new SqlConnection(db.connetionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("update remanproduct SET tca_oldpartno='" + TextBox1.Text + "', tca_newpartno='" + TextBox2.Text + "', oem_name='" + TextBox3.Text + "', oem_partnumber='" + TextBox4.Text + "', segment='" + TextBox5.Text + "',sub_segment='" + TextBox6.Text + "',tc_model='" + TextBox7.Text + "',engine='" + TextBox8.Text + "',application='" + TextBox9.Text + "',  alpha='" + TextBox10.Text + "',beta='" + TextBox11.Text + "',stroking_pressure='" + TextBox12.Text + "',setting_pressure='" + TextBox13.Text + "',lift='" + TextBox14.Text + "',Status='" + DropDownList1.SelectedValue + "', WhatsNew_status='" + DropDownList2.SelectedValue + "',updatedate='"+ Date +"' where remanID='" + updateProduct + "'", con);
            cmd.ExecuteNonQuery();
            con.Close();

            Response.Write("<script language=javascript>alert('Updated Record Successfully');window.location=('listremanproductlist.aspx');</script>");

        }

        }
    }
