﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="listremanproductlist.aspx.cs" Inherits="TurboProductCatalogue.RemanProduct.listremanproductlist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
     
.pagination {
  display: inline-block;
}

.pagination a {
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
}

.pagination a.active {
  background-color: #4CAF50;
  color: black;
  border-radius: 5px;
}

.pagination a:hover:not(.active) {
  background-color: #ddd;
  border-radius: 5px;
}
a:hover {
  background-color: green;
}
  </style> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section class="content-header">
        <h1>
            <i class="fa fa-link "></i>&nbsp;List of Reman Product 
        </h1>
    </section>   
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pnlMSG" runat="server" Visible="false">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblMSG" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>

                 <div class="well">
                    <div class="row">
                        <div class="col-md-4 ">
                            <asp:TextBox runat="server" ID="txt_search" placeholder="Type Something ..." CssClass="form-control"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator ID="req" runat="server" ErrorMessage="Type Something ..." ControlToValidate="txt_search" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                        </div>
                        <div class="col-md-4 ">
                            <asp:Button class="btn btn-primary" ID="Button1" ValidationGroup="btnSearch" Text="Search" runat="server" OnClick="btn_submit_Click" />


                        </div>
                    </div>
                </div>


                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of Reman Product</h3>
                        <div class="box-tools">
                            <a href="uploadremanproductlist.aspx" class="btn btn-success btn-sm">Upload Price</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                      
                            <asp:Repeater ID="rptPriceList" runat="server"  OnItemCommand="rptPriceList_ItemCommand" >
                                <HeaderTemplate>
                                    <table id="myTable" class="table table-hover table-bordered">
                                        <tr class="success">
                                            <th class="text-center" style="width: 5%">S.No</th>
                                            <th class="text-center" style="width: 10%">TCA OldPartNo</th>
                                            <th class="text-center" style="width: 10%">TCA NewPartNo</th>
                                            <th class="text-center" style="width: 25%">OEM Name</th>
                                            <th class="text-center" style="width: 15%">OEM Part No</th>
                                           <th class="text-center" style="width: 10%">Segment</th>
                                            <th class="text-center" style="width: 15%">Sub Segment</th>
                                          <th class="text-center" style="width: 40%"> Action</th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                 
                                     <tr>
                                        <td class="text-center">
                                            <%# Container.ItemIndex +1 %>

                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"tca_oldpartno")%>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"tca_newpartno")%>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"oem_name")%>
                                        </td>
                                        <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"oem_partnumber")%>
                                        </td>
                                        <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"segment")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"sub_segment")%>
                                        </td>
                                        <td class="text-center">
                                            <a href="updateremanproductlist.aspx?mode=edit&remanID=<%# TurboProductCatalogue.MyCrypto.GetEncryptedQueryString   (DataBinder.Eval(Container.DataItem,"remanID").ToString())%>" title="Edit RemanProduct" data-toggle="tooltip" data-placement="top" class="btn btn-default btn-xs btn-warning "><i class="fa fa-edit iconColor"></i></a>
                                            <a href="viewremanproductlist.aspx?mode=edit&remanID=<%# TurboProductCatalogue.MyCrypto.GetEncryptedQueryString ( DataBinder.Eval(Container.DataItem,"remanID").ToString())%>" title="View RemanProduct" data-toggle="tooltip" data-placement="top" class="btn btn-primary btn-xs btn-primary "><i class="fa fa-eye iconColor"></i></a>
                                            
                                             <asp:LinkButton ID="lnkDelete" ToolTip="Delete" CommandName="delete" CommandArgument='<%# Eval("remanID") %>'  class="btn btn-default btn-xs btn-danger" data-toggle="tooltip" data-placement="top" runat="server" Text='Delete User' OnClientClick='javascript:return confirm("Are you sure you want to delete?")'><i class="fa fa-trash iconColor"></i></asp:LinkButton>
                                        </td>

                                    </tr>
                               
  
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>

                            </asp:Repeater>
                              <div class="pagination" >
       <asp:Repeater ID="Repeater2" runat="server" OnItemCommand="Repeater2_ItemCommand">  
                <ItemTemplate>  
                    <asp:LinkButton ID="lnkPage" 
                         onmouseover="this.style.background='Green';" onmouseout="this.style.background='white';" 
                        Style="padding: 8px; margin: 2px; background: lightgray; border: solid 1px #666; color: black; font-weight: bold"  
                        CommandName="Page" CommandArgument="<%# Container.DataItem %>" runat="server" Font-Bold="True"><%# Container.DataItem %>  
                    </asp:LinkButton>  
                </ItemTemplate>  
            </asp:Repeater>  
        </div>
                            

                            <asp:Panel ID="pnlNoRecord" Visible="false" runat="server">
                                <h3 class="text-danger text-bold text-center">No Record Found</h3>
                            </asp:Panel>
                        </div>
                    </div>




                </div>
            </div>
            
        </div>
    </section>
</asp:Content>
