﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="uploadallmakepricelist.aspx.cs" Inherits="TurboProductCatalogue.RemanProduct.uploadallmakepricelist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>
            <i class="fa fa-cloud-upload"></i>&nbsp;Upload Reman Price List
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <div class="box  box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Upload Excel 
                        </h3>
                        <div class="box-tools">
                            <asp:Button ID="btnExcelTemplate"  runat="server" CssClass="btn btn-danger btn-sm" Text="Download Excel Template" OnClick="btnExcelTemplate_Click" />&nbsp;
                            <a href="listallmakepricelist.aspx" class="pull-right btn btn-success btn-sm">List Reman Price</a>                            
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>File Upload <span class="text-danger">*</span></label>
                                <asp:FileUpload ID="fileUpload" runat="server"  />
                                <asp:RequiredFieldValidator ID="reqDesc" Display="Dynamic" ForeColor="Red" ValidationGroup="btnUpload" ControlToValidate="fileUpload" runat="server" ErrorMessage='Choose Excel'></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Insert Mode <span class="text-danger">*</span></label>
                                <asp:DropDownList CssClass="form-control" ID="ddlInsertMode" runat="server">
                                    <asp:ListItem Value="0">Select Insert mode</asp:ListItem>
                                    <asp:ListItem Value="Append">Append</asp:ListItem>
                                    <asp:ListItem Value="Override">Override</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqSatus" Display="Dynamic" ValidationGroup="btnUpload" ForeColor="Red" InitialValue="0" ControlToValidate="ddlInsertMode" runat="server" ErrorMessage='Choose Insert Mode'></asp:RequiredFieldValidator>
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                        <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" CssClass="btn btn-success pull-right" Text="Upload" />
                    </div>

                    <asp:Label ID="Label1" runat="server" Text="" />

                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" style="text-align:center;" PageSize="15" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical" Height="333px" Width="507px">
                        <AlternatingRowStyle BackColor="#DCDCDC" />
    <Columns>
    <asp:BoundField DataField="oldpartno" HeaderText="OldPartNo" />
    <asp:BoundField DataField="newpartno" HeaderText="NewPartNo" />
    <asp:BoundField DataField="description" HeaderText="Description" />
        <asp:BoundField DataField="OEM_Name" HeaderText="OEM Name" />
    <asp:BoundField DataField="listprice" HeaderText="Sales Price" />
    <asp:BoundField DataField="mrp" HeaderText="MRP" />
        <asp:BoundField DataField="Status" HeaderText="Status" />
    </Columns>
                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>               


                </div>
            </div>
        </div>
    </section>
</asp:Content>
