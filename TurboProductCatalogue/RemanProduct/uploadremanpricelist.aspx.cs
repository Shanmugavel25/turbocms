﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.RemanProduct
{
    public partial class uploadremanpricelist : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            // btnExcelTemplate.Visible = false;
        }


        protected void btnUpload_Click(object sender, EventArgs e)
        {
            //if (fileUpload.HasFile)
            //{
            //    string FileName = Path.GetFileName(fileUpload.PostedFile.FileName);
            //    string Extension = Path.GetExtension(fileUpload.PostedFile.FileName);
            //    string FolderPath = ConfigurationManager.AppSettings["FolderPath"];                
            //    string FilePath = Server.MapPath("~/Upload/" + FileName);
            //    fileUpload.SaveAs(FilePath);
            //    Import_To_Grid(FilePath, Extension, ddlInsertMode.SelectedValue);
            //}
            String oldpartno;
            String newpartno;
            String description;
            String listprice;
            String mrplist;


            if (ddlInsertMode.SelectedValue == "Append")
            {

                string path = Path.GetFileName(fileUpload.FileName);

                path = path.Replace(" ", "");
                fileUpload.SaveAs(Server.MapPath("~/AllMakeUpload/") + path);
                String ExcelPath = Server.MapPath("~/AllMakeUpload/") + path;
                OleDbConnection mycon = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelPath + ";Extended Properties=Excel 8.0;Persist Security Info=False");
                mycon.Open();
                OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", mycon);
                OleDbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    oldpartno = dr[0].ToString();
                    newpartno = dr[1].ToString();
                    description = dr[2].ToString();
                    listprice = dr[3].ToString();
                    mrplist = dr[4].ToString();
                    savedata(oldpartno, newpartno, description, listprice, mrplist);
                }

                Label1.Text = "Data Has Been Saved Successfully";
                BindGridView();
            }
            else if (ddlInsertMode.SelectedValue == "Override")
            {
                SqlConnection con = new SqlConnection(db.connetionString);
                con.Open();
                SqlCommand cmd1 = new SqlCommand("Delete  from remanpricelist", con);
                cmd1.ExecuteNonQuery();

                string path = Path.GetFileName(fileUpload.FileName);
                path = path.Replace(" ", "");
                fileUpload.SaveAs(Server.MapPath("~/AllMakeUpload/") + path);
                String ExcelPath = Server.MapPath("~/AllMakeUpload/") + path;
                OleDbConnection mycon = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelPath + ";Extended Properties=Excel 8.0;Persist Security Info=False");
                mycon.Open();
                OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", mycon);
                OleDbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    oldpartno = dr[0].ToString();
                    newpartno = dr[1].ToString();
                    description = dr[2].ToString();
                    listprice = dr[3].ToString();
                    mrplist = dr[4].ToString();
                    savedata(oldpartno, newpartno, description, listprice, mrplist);
                }

                Label1.Text = "Data Has Been Saved Successfully";
                BindGridView();


            }
            else
            {
                Response.Write("<script language='javascript'>alert('Select a uploadedtype')</script>");
            }

        }
        private void BindGridView()
        {
            SqlConnection con1 = new SqlConnection(db.connetionString);
            con1.Open();
            SqlCommand cmd2 = new SqlCommand("select oldpartno,newpartno,description,listprice,mrp from remanpricelist", con1);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable ds = new DataTable();
            da.Fill(ds);
            GridView1.DataSource = ds;
            GridView1.EmptyDataText = "No Data Found";
            GridView1.DataBind();
            con1.Close();

            btnExcelTemplate.Visible = true;
        }
        private void savedata(String oldpartno1, String newpartno1, String description1, String listprice1, String mrplist1)
        {
            string Date = DateTime.Now.ToString();
            SqlConnection conn = new SqlConnection(db.connetionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("insert into remanpricelist(oldpartno,newpartno,description,listprice,mrp,insertedDate)Values('" + oldpartno1 + "','" + newpartno1 + "','" + description1 + "','" + listprice1 + "','" + mrplist1 + "','"+ Date +"')", conn);


            cmd.ExecuteNonQuery();
            conn.Close();


        }
        //public override void VerifyRenderingInServerForm(Control control)
        //{

        //}

        protected void btnExcelTemplate_Click(object sender, EventArgs e)
        {
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=remanpricelist_template.xlsx");
            Response.WriteFile(Server.MapPath("~/AllMakeUpload/remanpricelist_template.xlsx"));
            Response.Flush();
            Response.End();

            //ExportGridToExcel();
        }
    }
}