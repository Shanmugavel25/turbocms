﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace TurboProductCatalogue.RemanProduct
{
    public partial class updateallmakepricelist : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string RePrice = MyCrypto.GetDecryptedQueryString(Request.QueryString["allmakelistID"].ToString());
                //Session["RePrice"] = MyCrypto.GetDecryptedQueryString(Request.QueryString["remanpricelistID"].ToString());

                SqlConnection con = new SqlConnection(db.connetionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("select allmakelistID,oldpartno,newpartno,description,listprice,mrp,Status,OEM_Name from allmakepricelist where allmakelistID='" + RePrice + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    TextBox1.Text = sdr["oldpartno"].ToString();
                    TextBox2.Text = sdr["newpartno"].ToString();
                    TextBox3.Text = sdr["description"].ToString();
                    TextBox4.Text = sdr["listprice"].ToString();
                    TextBox5.Text = sdr["mrp"].ToString();
                    DropDownList1.SelectedValue = sdr["Status"].ToString();
                    TextBox6.Text = sdr["OEM_Name"].ToString();
                }
                con.Close();

            }

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string reprice = MyCrypto.GetDecryptedQueryString(Request.QueryString["allmakelistID"].ToString());
            SqlConnection con = new SqlConnection(db.connetionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("update allmakepricelist SET oldpartno='" + TextBox1.Text + "',newpartno='" + TextBox2.Text + "',description='" + TextBox3.Text + "',listprice='" + TextBox4.Text + "',mrp='" + TextBox5.Text + "',updateddate='" + DateTime.Now.ToString() + "',Status='" + DropDownList1.SelectedValue + "',OEM_Name='"+ TextBox6.Text +"' where allmakelistID='" + reprice + "'", con);
            cmd.ExecuteNonQuery();
            con.Close();

            Response.Write("<script language=javascript>alert('Updated Record Successfully');window.location=('listremanpricelist.aspx');</script>");

        }
    }
}