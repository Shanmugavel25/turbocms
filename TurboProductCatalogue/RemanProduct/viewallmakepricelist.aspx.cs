﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.RemanProduct
{
    public partial class viewallmakepricelist : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string remPrice = MyCrypto.GetDecryptedQueryString(Request.QueryString["allmakelistID"].ToString());
                //Session["remPrice"] = MyCrypto.GetDecryptedQueryString(Request.QueryString["remanpricelistID"].ToString());

                SqlConnection con = new SqlConnection(db.connetionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("select allmakelistID,oldpartno,newpartno,description,listprice,mrp,Status,OEM_Name from allmakepricelist where allmakelistID='" + remPrice + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    Label1.Text = sdr["oldpartno"].ToString();
                    Label2.Text = sdr["newpartno"].ToString();
                    Label3.Text = sdr["description"].ToString();
                    Label4.Text = sdr["listprice"].ToString();
                    Label5.Text = sdr["mrp"].ToString();
                    Label6.Text = sdr["Status"].ToString();
                    Label7.Text = sdr["OEM_Name"].ToString();
                }
                con.Close();

            }
        }
    }
}