﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.RemanProduct
{
    public partial class listallmakepricelist : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        private int iPageSize = 50;
        public bool isSearch = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            string strMSG;
            strMSG = Request.QueryString["msg"];
            if (!string.IsNullOrEmpty(strMSG))
            {
                pnlMSG.Visible = true;
                lblMSG.Text = strMSG;
            }
            if (!IsPostBack)
            {
                this.getData(1, "selectList");
            }
        }

        protected void getData(int iPageIndex, string qtype)
        {
            string conString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            SqlConnection sqlCon = new SqlConnection(conString);
            sqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand("sp_b_allmakeprice", sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@PageIndex", iPageIndex);
            sqlCmd.Parameters.AddWithValue("@PageSize", iPageSize);
            sqlCmd.Parameters.AddWithValue("@qtype", qtype);
            sqlCmd.Parameters.AddWithValue("@search", txt_search.Text.Trim());
            sqlCmd.Parameters.Add("@RecordCount", SqlDbType.Int, 4);
            sqlCmd.Parameters["@RecordCount"].Direction = ParameterDirection.Output;
            IDataReader iDr = sqlCmd.ExecuteReader();
            rptPriceList.DataSource = iDr;
            rptPriceList.DataBind();
            iDr.Close();
            sqlCon.Close();
            int iRecordCount = Convert.ToInt32(sqlCmd.Parameters["@RecordCount"].Value);

            double dPageCount = (double)((decimal)iRecordCount / Convert.ToDecimal(iPageSize));
            int iPageCount = (int)Math.Ceiling(dPageCount);
            List<ListItem> lPages = new List<ListItem>();
            if (iPageCount > 0)
            {
                for (int i = 1; i <= iPageCount; i++)
                    lPages.Add(new ListItem(i.ToString(), i.ToString(), i != iPageIndex));
            }
            Repeater2.DataSource = lPages;
            Repeater2.DataBind();
        }
       
        //catch (Exception ex)
        //{
        //    Response.Write(ex);
        //    pnlerror.Visible = true;
        //    lblError.Text = "Error Occured please Contact Administrator";
        //    noError = false;
        //}
        //finally
        //{
        //    conn.Close();
        //}



        protected void rptPriceList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                int notifyID = Convert.ToInt32(e.CommandArgument.ToString());
                string rMSG = "";
                rMSG = deleteFn(notifyID);
                if (noError)
                {
                    Response.Redirect("listallmakepricelist.aspx?msg=" + rMSG);
                }
                else
                {
                    pnlerror.Visible = true;
                    lblError.Text = "Error Occured please Contact Administrator";
                    noError = false;
                }
            }
        }
      

        protected string deleteFn(int allmakelistID)
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            string rStatus = "";
            try
            {
                Cmd = new SqlCommand("sp_b_allmakeprice", conn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("@allmakelistID", SqlDbType.Int).Value = allmakelistID;
                Cmd.Parameters.Add("@qtype", SqlDbType.VarChar, 20).Value = "del";
                Cmd.Parameters.Add("@MSG", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                conn.Open();
                Cmd.ExecuteNonQuery();
                rStatus = Cmd.Parameters["@MSG"].Value.ToString();
                conn.Close();
            }
            catch (Exception ex)
            {
                // Response.Write(ex);
                pnlerror.Visible = true;
                lblError.Text = "Error Occured please Contact Administrator";
                noError = false;
            }
            return rStatus;
        }

        protected string inr(string mrp)
        {
            double price = Convert.ToDouble(mrp);
            return string.Format("{0:#.00}", price);
        }
        protected void btn_submit_Click(object sender, EventArgs e)
        {

            this.isSearch = true;
            this.getData(1, "search");
        }

        public void bind_product()
        {
            rptPriceList.DataSource = Search();
            rptPriceList.DataBind();
        }
        protected DataTable Search()
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            string value = txt_search.Text;
            string query = string.Empty;
            if (!string.IsNullOrEmpty(value))
                query = "select ROW_NUMBER() OVER (ORDER BY allmakelistID)[RowNumber],* from allmakepricelist where  newpartno like '%" + value + "%'or oldpartno like '%" + value + "%'or description like '%" + value + "%'or listprice like '%" + value + "%' or mrp like '%" + value + "%'";
            else
                query = "select ROW_NUMBER() OVER (ORDER BY allmakelistID)[RowNumber],* from allmakepricelist";
            SqlDataAdapter sda1 = new SqlDataAdapter(query, conn);
            DataTable dt = new DataTable();
            sda1.Fill(dt);
            return dt;
        }
        protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int iPageIndex = Convert.ToInt32(e.CommandArgument);
            if (this.isSearch)
            {
                this.getData(iPageIndex, "search");
            }
            else
            {
                this.getData(iPageIndex, "selectList");
            }

        }
    }
}