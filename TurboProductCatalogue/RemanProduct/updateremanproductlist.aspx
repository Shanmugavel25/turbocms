﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="updateremanproductlist.aspx.cs" Inherits="TurboProductCatalogue.RemanProduct.updateremanproductlist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>
            <i class="fa fa-bell "></i>&nbsp;Update Reman Product Catalog
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-10">
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <div class="box  box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="lblHead" runat="server" Text=""></asp:Label>
                            Reman Product Catalog</h3>   
                        <div class="box-tools">
                            <a href="listremanproductlist.aspx" class="pull-right btn btn-success btn-sm">List Product Catalog</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>TCA OldPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox1"    MaxLength="50" CssClass="form-control" ReadOnly="true" placeholder="Enter TCA OldpartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="reqTitle" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox1" runat="server" ErrorMessage='Enter OldpartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>TCA NewPartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox2"    MaxLength="50" CssClass="form-control" ReadOnly="true"  placeholder="Enter TCA NewPartNo" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox2" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>OEM Name <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox3"  MaxLength="50" CssClass="form-control" placeholder="Enter OEM Name" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox3" runat="server" ErrorMessage='Enter OEM Name'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>OEM PartNo <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox4"  MaxLength="50" CssClass="form-control" placeholder="Enter OEM PartNo" runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox4" runat="server" ErrorMessage='Enter PartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Segment <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox5"  MaxLength="50" CssClass="form-control" placeholder="Enter Segment" runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Sub Segment <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox6"  MaxLength="50" CssClass="form-control" placeholder="Enter SubSegment" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox6" runat="server" ErrorMessage='Enter SubSegment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>TC Model <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox7"  MaxLength="50" CssClass="form-control" placeholder="Enter TC Model" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox7" runat="server" ErrorMessage='Enter Model'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Engine <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox8"  MaxLength="50" CssClass="form-control" placeholder="Enter Engine" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox8" runat="server" ErrorMessage='Enter Engine'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Application <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox9"  MaxLength="50" CssClass="form-control" placeholder="Enter Application" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox9" runat="server" ErrorMessage='Enter Application'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                       

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Alpha <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox10"  MaxLength="50" CssClass="form-control" placeholder="Enter Alpha " runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator27" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox28" runat="server" ErrorMessage='Enter Alpha'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Beta <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox11"  MaxLength="50" CssClass="form-control" placeholder="Enter Beta" runat="server"></asp:TextBox>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator28" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox29" runat="server" ErrorMessage='Enter Beta'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Storking Pressure <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox12"  MaxLength="50" CssClass="form-control" placeholder="Enter Pressure" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator29" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox30" runat="server" ErrorMessage='Enter Pressure'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Setting Pressure <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox13"  MaxLength="50" CssClass="form-control" placeholder="Enter Pressure" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator30" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox31" runat="server" ErrorMessage='Enter Pressure'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Lift <span class="text-danger">*</span></label>
                                <asp:TextBox ID="TextBox14"  MaxLength="50" CssClass="form-control" placeholder="Enter Lift" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator31" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox32" runat="server" ErrorMessage='Enter Lift'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                       <div class="col-md-6">
                            <div class="form-group">
                                <label>Status<span class="text-danger">*</span></label>
                                <%--<asp:TextBox ID="TextBox33"  MaxLength="50" CssClass="form-control" placeholder="Enter Staus" runat="server"></asp:TextBox>
                                --%><asp:DropDownList ID="DropDownList1" MaxLength="50" CssClass="form-control" runat="server">
                                    <asp:ListItem Value="Active"> Active</asp:ListItem>
                                    <asp:ListItem Value="Deactive">Deactive</asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator31" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox32" runat="server" ErrorMessage='Enter Lift'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> New Status <span class="text-danger">*</span></label>
                                <%--<asp:TextBox ID="TextBox34"  MaxLength="50" CssClass="form-control" placeholder="Enter New Status" runat="server"></asp:TextBox>
                                --%><asp:DropDownList ID="DropDownList2" MaxLength="50" CssClass="form-control" runat="server">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator31" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox32" runat="server" ErrorMessage='Enter Lift'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        

                        

                    </div>
                    <div class="box-footer">
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="btn btn-success pull-right" Text="Submit" />
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
