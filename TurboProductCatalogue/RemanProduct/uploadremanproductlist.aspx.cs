﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.RemanProduct
{
    public partial class uploadremanproductlist : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            //if (fileUpload.HasFile)
            //{
            //    string FileName = Path.GetFileName(fileUpload.PostedFile.FileName);
            //    string Extension = Path.GetExtension(fileUpload.PostedFile.FileName);
            //    string FolderPath = ConfigurationManager.AppSettings["FolderPath"];                
            //    string FilePath = Server.MapPath("~/Upload/" + FileName);
            //    fileUpload.SaveAs(FilePath);
            //    Import_To_Grid(FilePath, Extension, ddlInsertMode.SelectedValue);
            //}
            String tca_oldpartno;
            String tca_newpartno;
            String oem_name;
            String oem_partnumber;
            String segment;
            String sub_segment;
            String tc_model;
            String engine;
            String application;
            String alpha;
            String beta;
            String stroking_pressure;
            String setting_pressure;
            String lift;
            String Status;
            String WhatsNew_status;

             if (ddlInsertMode.SelectedValue == "Append")
            {

                string path = Path.GetFileName(fileUpload.FileName);

                path = path.Replace(" ", "");
                fileUpload.SaveAs(Server.MapPath("~/Upload1/") + path);
                String ExcelPath = Server.MapPath("~/Upload1/") + path;
                OleDbConnection mycon = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelPath + ";Extended Properties=Excel 8.0;Persist Security Info=False");
                mycon.Open();
                OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", mycon);
                OleDbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    tca_oldpartno = dr[0].ToString();
                    tca_newpartno = dr[1].ToString();
                    oem_name = dr[2].ToString();
                    oem_partnumber = dr[3].ToString();
                    segment = dr[4].ToString();
                    sub_segment = dr[5].ToString();
                    tc_model = dr[6].ToString();
                    engine = dr[7].ToString();
                    application = dr[8].ToString();
                    alpha = dr[9].ToString();
                    beta = dr[10].ToString();
                    stroking_pressure = dr[11].ToString();
                    setting_pressure = dr[12].ToString();
                    lift = dr[13].ToString();
                    Status = dr[14].ToString();
                    WhatsNew_status = dr[15].ToString();

                    savedata(tca_oldpartno,tca_newpartno,
                 oem_name,
                 oem_partnumber,
                 segment,
                 sub_segment,
                 tc_model,
                 engine,
                 application,
                 alpha,
                 beta,
                 stroking_pressure,
                 setting_pressure,
                 lift, Status, WhatsNew_status);
                }
                 Label1.Text = "Data Has Been Saved Successfully";
                BindGridView();
             }
            else if (ddlInsertMode.SelectedValue == "Override")
            {
                SqlConnection con = new SqlConnection(db.connetionString);
                con.Open();
                SqlCommand cmd1 = new SqlCommand("Delete  from remanproduct", con);
                cmd1.ExecuteNonQuery();

                string path = Path.GetFileName(fileUpload.FileName);

                path = path.Replace(" ", "");
                fileUpload.SaveAs(Server.MapPath("~/Upload1/") + path);
                String ExcelPath = Server.MapPath("~/Upload1/") + path;
                OleDbConnection mycon = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelPath + ";Extended Properties=Excel 8.0;Persist Security Info=False");
                mycon.Open();
                OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", mycon);
                OleDbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    tca_oldpartno = dr[0].ToString();
                    tca_newpartno = dr[1].ToString();
                    oem_name = dr[2].ToString();
                    oem_partnumber = dr[3].ToString();
                    segment = dr[4].ToString();
                    sub_segment = dr[5].ToString();
                    tc_model = dr[6].ToString();
                    engine = dr[7].ToString();
                    application = dr[8].ToString();
                    alpha = dr[9].ToString();
                    beta = dr[10].ToString();
                    stroking_pressure = dr[11].ToString();
                    setting_pressure = dr[12].ToString();
                    lift = dr[13].ToString();
                    Status = dr[14].ToString();
                    WhatsNew_status = dr[15].ToString();

                    savedata(tca_oldpartno,tca_newpartno,
                 oem_name,
                 oem_partnumber,
                 segment,
                 sub_segment,
                 tc_model,
                 engine,
                 application,
                 alpha,
                 beta,
                 stroking_pressure,
                 setting_pressure,
                 lift, Status, WhatsNew_status);
                }
                 Label1.Text = "Data Has Been Saved Successfully";
                BindGridView();
             }
             else
             {
                 Response.Write("<script language='javascript'>alert('Select a uploadedtype')</script>");
             }

        }
        private void BindGridView()
        {
            SqlConnection con1 = new SqlConnection(db.connetionString);
            con1.Open();
            SqlCommand cmd2 = new SqlCommand("Select tca_oldpartno, tca_newpartno, oem_name, oem_partnumber, segment,sub_segment,tc_model,engine,application,alpha,beta,stroking_pressure,setting_pressure,lift,Status,WhatsNew_status from remanproduct", con1);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable ds = new DataTable();
            da.Fill(ds);
            GridView1.DataSource = ds;
            GridView1.EmptyDataText = "No Data Found";
            GridView1.DataBind();
            con1.Close();


        }
        private void savedata(String tca_oldpartno1,String tca_newpartno1,String oem_name1,String oem_partnumber1,String segment1,String sub_segment1,String tc_model1,
        String engine1,
        String application1,String alpha1,
        String beta1,
        String stroking_pressure1,
        String setting_pressure1,
        String lift1, String Status1, String WhatsNew_status1)
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand("insert into remanproduct(tca_oldpartno,tca_newpartno,oem_name,oem_partnumber,segment,sub_segment,tc_model,engine,application,alpha,beta,stroking_pressure,setting_pressure,lift,Status,WhatsNew_status)Values('" + tca_oldpartno1 + "','" + tca_newpartno1 + "','" + oem_name1 + "','" + oem_partnumber1 + "','" + segment1 + "','" + sub_segment1 + "','" + tc_model1 + "','" + engine1 + "','" + application1 + "','" + alpha1 + "','" + beta1 + "','" + stroking_pressure1 + "','" + setting_pressure1 + "','" + lift1 + "','" + Status1 + "','" + WhatsNew_status1 + "')", conn);
            cmd.ExecuteNonQuery();
            conn.Close();


        }
        protected void btnExcelTemplate_Click(object sender, EventArgs e)
        {
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=remanproductlist_template.xlsx");
            Response.WriteFile(Server.MapPath("~/Upload1/remanproductlist_template.xlsx"));
            Response.Flush();
            Response.End();
        }
    }
}