﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="uploadremanproductlist.aspx.cs" Inherits="TurboProductCatalogue.RemanProduct.uploadremanproductlist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>
            <i class="fa fa-cloud-upload"></i>&nbsp;Upload Reman Product 
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <div class="box  box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Upload Excel 
                        </h3>
                        <div class="box-tools">
                            <asp:Button ID="btnExcelTemplate"  runat="server" CssClass="btn btn-danger btn-sm" Text="Download Excel Template" OnClick="btnExcelTemplate_Click" />&nbsp;
                            <a href="listremanproductlist.aspx" class="pull-right btn btn-success btn-sm">List Reman Product Catalog</a>                            
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>File Upload <span class="text-danger">*</span></label>
                                <asp:FileUpload ID="fileUpload" runat="server"  />
                                <asp:RequiredFieldValidator ID="reqDesc" Display="Dynamic" ForeColor="Red" ValidationGroup="btnUpload" ControlToValidate="fileUpload" runat="server" ErrorMessage='Choose Excel'></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Insert Mode <span class="text-danger">*</span></label>
                                <asp:DropDownList CssClass="form-control" ID="ddlInsertMode" runat="server">
                                    <asp:ListItem Value="0">Select Insert mode</asp:ListItem>
                                    <asp:ListItem Value="Append">Append</asp:ListItem>
                                    <asp:ListItem Value="Override">Override</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqSatus" Display="Dynamic" ValidationGroup="btnUpload" ForeColor="Red" InitialValue="0" ControlToValidate="ddlInsertMode" runat="server" ErrorMessage='Choose Insert Mode'></asp:RequiredFieldValidator>
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                        <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" CssClass="btn btn-success pull-right" Text="Upload" />
                    </div>

                    <asp:Label ID="Label1" runat="server" Text="" />

                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="100" CellPadding="3" style="text-align:center;" GridLines="Vertical" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Height="380px" Width="2945px">
                       <Columns>
                            <asp:BoundField DataField="tca_oldpartno" HeaderText=" TCA OldPartNo" />
                            <asp:BoundField DataField="tca_newpartno" HeaderText="TCA NewPartNo" />
                            <asp:BoundField DataField="oem_name" HeaderText="OEM Name" />
                            <asp:BoundField DataField="oem_partnumber" HeaderText="OEM PartNumber" />
                            <asp:BoundField DataField="segment" HeaderText="Segment" />
                            <asp:BoundField DataField="sub_segment" HeaderText="Sub Segment" />
                            <asp:BoundField DataField="tc_model" HeaderText="TC Model" />
                            <asp:BoundField DataField="engine" HeaderText="Engine" />
                            <asp:BoundField DataField="application" HeaderText="Application" />
                            <%--<asp:BoundField DataField="coreassemblykit_oldpartno" HeaderText="Core Assembly Kit OldpartNo" />
                            <asp:BoundField DataField="coreassemblykit_newpartno" HeaderText="Core Assembly Kit NewPartNo" />
                            <asp:BoundField DataField="overhaulkit_oldpartno" HeaderText="OverHaul Kit OldPartNo" />
                            <asp:BoundField DataField="overhaulkit_newpartno" HeaderText="OverHaul Kit NewPartNo" />
                            <asp:BoundField DataField="backplate_oldpartno" HeaderText="Back Plate OldPartNo" />
                            <asp:BoundField DataField="backplate_newpartno" HeaderText="Back Plate NewPartNo" />
                            <asp:BoundField DataField="secondarykit_oldpartno" HeaderText="Secondary Kit OldPartNo" />
                            <asp:BoundField DataField="secondarykit_newpartno" HeaderText="Secondary Kit NewPartNo" />
                            <asp:BoundField DataField="centralhousing_oldpartno" HeaderText="Central Housing OldPartNo" />
                            <asp:BoundField DataField="centralhousing_newpartno" HeaderText="Central Housing NewPartNo" />
                            <asp:BoundField DataField="compressorwheel_oldpartno" HeaderText="Compressor Wheel OldPartNo" />
                            <asp:BoundField DataField="compressorwheel_newpartno" HeaderText="Compressor Wheel NewPartNo " />
                            <asp:BoundField DataField="actuatorassembly_oldpartno" HeaderText="Actuator Assembly OldpartNo" />
                            <asp:BoundField DataField="actuatorassembly_newpartno" HeaderText="Actuator Assembly NewPartNo" />
                            <asp:BoundField DataField="turbinewheel_oldpartno" HeaderText="Turbine Wheel OldPartNo" />
                            <asp:BoundField DataField="turbinewheel_newpartno" HeaderText="TUrbine Wheel NewPartNo" />
                            <asp:BoundField DataField="gasket_oldpartno" HeaderText="Gasket Kit OldpartNo" />
                            <asp:BoundField DataField="gasket_newpartno" HeaderText="Gasket Kit NewPartNo" />--%>
                            <asp:BoundField DataField="alpha" HeaderText="Alpha" />
                            <asp:BoundField DataField="beta" HeaderText="Beta" />
                            <asp:BoundField DataField="stroking_pressure" HeaderText="Stroking Pressure" />
                            <asp:BoundField DataField="setting_pressure" HeaderText="Setting Pressure" />
                            <asp:BoundField DataField="lift" HeaderText="Lift" />
                           <asp:BoundField DataField="Status" HeaderText="Status" />
                           <asp:BoundField DataField="WhatsNew_status" HeaderText="New Status" />
                        </Columns>

 
                        <AlternatingRowStyle BackColor="#DCDCDC" />
                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#000065" />
    
    </asp:GridView>               


                </div>
            </div>
        </div>
    </section>
</asp:Content>
