﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.RemanProduct
{
    public partial class viewremanproductlist : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string Productlist = MyCrypto.GetDecryptedQueryString(Request.QueryString["remanID"].ToString());
                //Session["Productlist"] = MyCrypto.GetDecryptedQueryString(Request.QueryString["remanID"].ToString());

                SqlConnection con = new SqlConnection(db.connetionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select remanID,tca_oldpartno, tca_newpartno, oem_name, oem_partnumber, segment,sub_segment,tc_model,engine,application,alpha,beta,stroking_pressure,setting_pressure,lift,Status,WhatsNew_status from remanproduct where remanID ='" + Productlist + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    Label1.Text = sdr["tca_oldpartno"].ToString();
                    Label2.Text = sdr["tca_newpartno"].ToString();
                    Label3.Text = sdr["oem_name"].ToString();
                    Label4.Text = sdr["oem_partnumber"].ToString();
                    Label5.Text = sdr["segment"].ToString();
                    Label6.Text = sdr["sub_segment"].ToString();
                    Label7.Text = sdr["tc_model"].ToString();
                    Label8.Text = sdr["engine"].ToString();
                    Label9.Text = sdr["application"].ToString();
                    Label10.Text = sdr["alpha"].ToString();
                    Label11.Text = sdr["beta"].ToString();
                    Label12.Text = sdr["stroking_pressure"].ToString();
                    Label13.Text = sdr["setting_pressure"].ToString();
                    Label14.Text = sdr["lift"].ToString();
                    Label15.Text = sdr["Status"].ToString();
                    Label16.Text = sdr["WhatsNew_status"].ToString();

                }
                con.Close();
            }
        }
    }
}