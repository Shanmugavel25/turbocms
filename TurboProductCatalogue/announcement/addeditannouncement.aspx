﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Masters/Site.Master" CodeBehind="addeditannouncement.aspx.cs" Inherits="TurboProductCatalogue.announcement.addeditannouncement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="content-header">
        <h1>
            <i class="fa fa-bullhorn"></i>&nbsp;Announcement
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-10">
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <div class="box  box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="lblHead" runat="server" Text=""></asp:Label>
                            Announcement</h3>   
                        <div class="box-tools">
                            <a href="listannouncement.aspx" class="pull-right btn btn-success btn-sm">List Announcement</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Description <span class="text-danger">*</span></label>
                                <asp:TextBox ID="txtDesc" placeholder="Enter Description" Columns="4" Rows="4" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqDesc" Display="Dynamic" ForeColor="Red" ControlToValidate="txtDesc" runat="server" ErrorMessage='Enter Description'></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Status <span class="text-danger">*</span></label>
                                <asp:DropDownList CssClass="form-control" ID="ddlStatus" runat="server">
                                    <asp:ListItem Value="Active">Active</asp:ListItem>
                                    <asp:ListItem Value="Deactive">Deactive</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqSatus" Display="Dynamic" ForeColor="Red" ControlToValidate="ddlStatus" runat="server" ErrorMessage='Select Status'></asp:RequiredFieldValidator>
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="btn btn-success pull-right" Text="Submit" />
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
