﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.announcement
{
    public partial class addeditannouncement : System.Web.UI.Page
    {
        public common db = new common();
        public int id = 0;
        public string mode = "";
        public bool noError = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            mode = Request.QueryString["mode"];
            id = Convert.ToInt32(Request.QueryString["id"]);
            if (!IsPostBack)
            {
                if (mode == "add")
                {
                    lblHead.Text = "Create New";
                }
                else if (mode == "edit")
                {
                    lblHead.Text = "Edit";
                    getDataByID(id);
                }
            }
        }

        protected void getDataByID(int id)
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            SqlDataReader sqlReader;

            try
            {
                Cmd = new SqlCommand("sp_b_announcement", conn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                Cmd.Parameters.Add("@qtype", SqlDbType.VarChar, 20).Value = "selectByID";
                conn.Open();
                sqlReader = Cmd.ExecuteReader();
                if (sqlReader.HasRows)
                {
                    sqlReader.Read();
                    txtDesc.Text = sqlReader["announce_desc"].ToString();
                    ddlStatus.SelectedValue = sqlReader["status"].ToString();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
                pnlerror.Visible = true;
                lblError.Text = "Error Occured, Please Contact Administrator";
                noError = false;
            }
            finally
            {
                conn.Close();
            }
        }

        protected string createUpAnnouncement(string qtype)
        {
            string rMSG = "";
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            try
            {
                Cmd = new SqlCommand("sp_b_announcement", conn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                Cmd.Parameters.Add("@announce_desc", SqlDbType.VarChar, 500).Value = txtDesc.Text.Trim();
                Cmd.Parameters.Add("@status", SqlDbType.VarChar, 10).Value = ddlStatus.SelectedValue;
                Cmd.Parameters.Add("@updatedatetime", SqlDbType.DateTime).Value = DateTime.Now;
                Cmd.Parameters.Add("@qtype", SqlDbType.VarChar, 50).Value = qtype;
                Cmd.Parameters.Add("@MSG", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                conn.Open();
                Cmd.ExecuteNonQuery();
                rMSG = Cmd.Parameters["@MSG"].Value.ToString();
                // Response.Write(rMSG)
                conn.Close();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
                pnlerror.Visible = true;
                lblError.Text = "Error Occured, Please Contact Administrator";
                noError = false;
            }

            return rMSG;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string msg = "";

            if (mode == "add")
                msg = createUpAnnouncement("ins");
            else if (mode == "edit")
                msg = createUpAnnouncement("update");

            if (noError)
            {
                Response.Redirect("listannouncement.aspx?msg=" + msg);
            }
            else
            {
                pnlerror.Visible = true;
                lblError.Text = "Error Occured please Contact Administrator";
            }
        }

    }
}