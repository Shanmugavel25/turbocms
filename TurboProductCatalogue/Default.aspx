﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TurboProductCatalogue.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Turbo Product Catalogue - Login</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/AdminLTE.min.css" />
</head>
<body class="hold-transition login-page">
    <form id="form1" runat="server">
        <div class="login-box">
            <div class="login-logo">
                <b style="font-size: 30px;">Turbo Product Catalague</b>
            </div>

            <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>

                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtUserName" CssClass="form-control" placeholder="Enter Your Username" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="req" runat="server" ControlToValidate="txtUserName"  Display="Dynamic" ErrorMessage="Enter Username" ForeColor="Red"></asp:RequiredFieldValidator>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtUserPassword" minlength="8" MaxLength="20" CssClass="form-control" placeholder="Enter Password" TextMode="Password" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  Display="Dynamic" ControlToValidate="txtUserPassword"  ForeColor="Red" errormessage="Password must be 8 characters and have both letters and numbers." validationexpression="(?=.{8,})[a-zA-Z]+[^a-zA-Z]+|[^a-zA-Z]+[a-zA-Z]+" ></asp:RequiredFieldValidator>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <asp:Button ID="btnSubmit" runat="server" Text="Sign In" OnClick="btnSubmit_Click" CssClass="btn btn-success btn-block btn-flat" />
                    </div>
                </div>

                <asp:Label ID="lblText" ForeColor="red" runat="server" Text=""></asp:Label>
            </div>


            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 2.2.3 -->
        <%--<script src="http://127.0.0.1/plugins/jquery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="http://127.0.0.1/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="http://127.0.0.1/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%'
                });
            });
        </script>--%>
    </form>
</body>
</html>
