﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" MasterPageFile="~/Masters/Site.Master" Inherits="TurboProductCatalogue.dashboard" %>
<%--<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
  
.card-box {
    position: relative;
    color: #fff;
    padding: 20px 10px 40px;
    margin: 20px 0px;
}
.card-box:hover {
    text-decoration: none;
    color: #f1f1f1;
}
.card-box:hover .icon i {
    font-size: 100px;
    transition: 1s;
    -webkit-transition: 1s;
}
.card-box .inner {
    padding: 5px 10px 0 10px;
}
.card-box h3 {
    font-size: 27px;
    font-weight: bold;
    margin: 0 0 8px 0;
    white-space: nowrap;
    padding: 0;
    text-align: left;
}
.card-box p {
    font-size: 15px;
}
.card-box .icon {
    position: absolute;
    top: auto;
    bottom: 5px;
    right: 5px;
    z-index: 0;
    font-size: 72px;
    color: rgba(0, 0, 0, 0.15);
}
.card-box .card-box-footer {
    position: absolute;
    left: 0px;
    bottom: 0px;
    text-align: center;
    padding: 3px 0;
    color: rgba(255, 255, 255, 0.8);
    background: rgba(0, 0, 0, 0.1);
    width: 100%;
    text-decoration: none;
}
.card-box:hover .card-box-footer {
    background: rgba(0, 0, 0, 0.3);
}
.bg-blue {
    background-color: #00c0ef !important;
}
.bg-green {
    background-color: #00a65a !important;
}
.bg-orange {
    background-color: #f39c12 !important;
}
.bg-red {
    background-color: #d9534f !important;
}

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="content-header">
        <h1>
            <i class="fa fa-dashboard "></i>&nbsp;Dashboard
        </h1>
    </section>
    <section>
        <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="card-box bg-blue">
                    <div class="inner">
                        <h2> <b> <asp:Label Id="label1" runat="server" Text=""></asp:Label> </b></h2>
                        <p><b>Total Registered Users</b> </p>
                    </div>
                    
                    <div class="icon">
                        <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="RegisterUser/listregisteruser.aspx" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="card-box bg-red">
                    <div class="inner">
                         <h2> <b> <asp:Label Id="label2" runat="server" Text=""></asp:Label></b> </h2>
                        <b><p> Approved Users </p></b>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                    </div>
                    <a href="RegisterUser/approveregister.aspx" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card-box bg-orange">
                    <div class="inner">
                        <h2> <b> <asp:Label Id="label3" runat="server" Text=""></asp:Label> </b></h2>
                        <b><p> Rejected Users </p></b>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user-times" aria-hidden="true"></i>
                    </div>
                    <a href="RegisterUser/rejectregister.aspx" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
           <%--<div class="col-lg-3 col-sm-6">
                <div class="card-box bg-green">
                    <div class="inner">
                        <h2> <b> <asp:Label Id="label4" runat="server" Text=""></asp:Label> </b>  </h2>
                        <p><b> Pending Users </b></p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user-secret"></i>
                    </div>
                    <a href="#" class="card-box-footer">View More <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>--%>
        </div>
    </div>
      <%-- <div>
         <asp:Chart ID="Chart1" runat="server" Width="562px" Height="333px" >
   
      
        <Series>
            <asp:Series Name="Series1" XValueMember="Registed Users" YValueMembers="Status">
            </asp:Series>
        </Series>
        <Legends>
        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
            LegendStyle="Column">
        </asp:Legend>
    </Legends>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1" BorderWidth="1">
            
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</div>--%>
   

        


    </section>

    

</asp:Content>
