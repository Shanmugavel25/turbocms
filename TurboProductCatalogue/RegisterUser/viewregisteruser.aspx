﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="viewregisteruser.aspx.cs" Inherits="TurboProductCatalogue.RegisterUser.viewregisteruser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section class="content-header">
        <h1>
            <i class="fa fa-bell "></i>&nbsp;View Registered Users
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-10">
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <div class="box  box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="lblHead" runat="server" Text=""></asp:Label>
                            View Users</h3>   
                        <div class="box-tools">
                            <a href="listregisteruser.aspx" class="pull-right btn btn-success btn-sm">List of Registered Users</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> Name  <span class="text-danger">:</span></label>
                                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                                <%--<asp:RequiredFieldValidator ID="reqTitle" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox1" runat="server" ErrorMessage='Enter OldpartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> Mobile No <span class="text-danger">:</span></label>
                                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox2" runat="server" ErrorMessage='Enter NewPartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> Email <span class="text-danger">:</span></label>
                               <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox3" runat="server" ErrorMessage='Enter OEM Name'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>State <span class="text-danger">:</span></label>
                               <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox4" runat="server" ErrorMessage='Enter PartNo'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label> City <span class="text-danger">:</span></label>
                                <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label> User Type <span class="text-danger">:</span></label>
                                <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label> Device Type <span class="text-danger">:</span></label>
                                <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label> Device Model <span class="text-danger">:</span></label>
                                <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label> Device Name  <span class="text-danger">:</span></label>
                                <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label> OTP <span class="text-danger">:</span></label>
                                <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label> Approve Status <span class="text-danger">:</span></label>
                                <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label> Approve Date <span class="text-danger">:</span></label>
                                <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label> Insert Date <span class="text-danger">:</span></label>
                                <asp:Label ID="Label13" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label> Update Date <span class="text-danger">:</span></label>
                                <asp:Label ID="Label14" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label> Password <span class="text-danger">:</span></label>
                                <asp:Label ID="Label15" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label> Remarks <span class="text-danger">:</span></label>
                                <asp:Label ID="Label16" runat="server" Text="Label"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ForeColor="Red" ControlToValidate="TextBox5" runat="server" ErrorMessage='Enter Segment'></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>


                     </div>
                   <%-- <div class="box-footer">
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="btn btn-success pull-right" Text="Submit" />
                    </div>--%>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
