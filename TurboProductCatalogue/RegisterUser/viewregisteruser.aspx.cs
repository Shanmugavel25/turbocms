﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.RegisterUser
{
    public partial class viewregisteruser : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string RegisterID = MyCrypto.GetDecryptedQueryString(Request.QueryString["userID"].ToString());
                //Session["RegisterID"] = MyCrypto.GetDecryptedQueryString(Request.QueryString["userID"].ToString());

                SqlConnection con = new SqlConnection(db.connetionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select userID,username,mobileno,email,state,city,usertype,devicetype,devicemodel,devicename,otp,approvestatus,approverdate,inserteddate,updateddate,password,remarks from RegisteredUsers where userID='" + RegisterID + "'", con);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    Label1.Text = sdr["username"].ToString();
                    Label2.Text = sdr["mobileno"].ToString();
                    Label3.Text = sdr["email"].ToString();
                    Label4.Text = sdr["state"].ToString();
                    Label5.Text = sdr["city"].ToString();
                    Label6.Text = sdr["usertype"].ToString();
                    Label7.Text = sdr["devicetype"].ToString();
                    Label8.Text = sdr["devicemodel"].ToString();
                    Label9.Text = sdr["devicename"].ToString();
                    Label10.Text = sdr["otp"].ToString();
                    Label11.Text = sdr["approvestatus"].ToString();
                    Label12.Text = sdr["approverdate"].ToString();
                    Label13.Text = sdr["inserteddate"].ToString();
                    Label14.Text = sdr["updateddate"].ToString();
                    Label15.Text = sdr["password"].ToString();
                    Label16.Text = sdr["remarks"].ToString();
                }
                con.Close();
            }
        }
    }
}