﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.RegisterUser
{
    public partial class rejectform : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string Register = MyCrypto.GetDecryptedQueryString(Request.QueryString["userID"].ToString());
                //Session["Register"] = MyCrypto.GetDecryptedQueryString(Request.QueryString["userID"].ToString());
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            string registerid = MyCrypto.GetDecryptedQueryString(Request.QueryString["userID"].ToString());
            string Reason = txtremarks.Text; 

            SqlConnection con = new SqlConnection(db.connetionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("Update RegisteredUsers SET approvestatus='Reject' , updateddate='" + DateTime.Now.ToString() + "',approverdate='" + DateTime.Now.ToString() + "',remarks='" + Reason + "' where userID='" + registerid + "'", con);
            cmd.ExecuteNonQuery();
            con.Close();

            Response.Redirect("listregisteruser.aspx");
               
        }
    }
}