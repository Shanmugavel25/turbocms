﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Collections;
using System.Web;
using System.Net.Mime;
using System.Text;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
using System.Web.UI.HtmlControls;

namespace TurboProductCatalogue.RegisterUser
{
    public partial class listregisteruser1 : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();

        protected void Page_Load(object sender, EventArgs e)
        {

            string strMSG;
            strMSG = Request.QueryString["msg"];
            if (!string.IsNullOrEmpty(strMSG))
            {
                pnlMSG.Visible = true;
                lblMSG.Text = strMSG;
            }
            if (!IsPostBack)
            {
                getData();
            }
            GeneratePassword();

            

        }

        protected void getData()
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            //SqlDataReader sqlReader;
            //try
            //{
            Cmd = new SqlCommand("sp_b_registeruser", conn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.Add("@qtype", SqlDbType.VarChar, 50).Value = "selectList";
            Cmd.Parameters.Add("@MSG", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
            conn.Open();

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            adp.Fill(dt);
            PagedDataSource pgitems = new PagedDataSource();
            DataView dv = new DataView(dt);
            pgitems.DataSource = dv;
            pgitems.AllowPaging = true;
            pgitems.PageSize = 15;
            pgitems.CurrentPageIndex = PageNumber;
            if (pgitems.PageCount > 1)
            {
                rptPaging.Visible = true;
                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPaging.DataSource = pages;
                rptPaging.DataBind();
            }
            else
            {

                rptPaging.Visible = false;

            }
            rptPriceList.DataSource = pgitems;
            rptPriceList.DataBind();

            conn.Close();
        }
        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            getData();
        }
      


        protected void rptPriceList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                Response.Write("<script language=javascript>alert('Account Activated Sucessfully User Credintal Sent to Registered Mail');window.location=('listregisteruser.aspx');</script>");

            }
            

        }
       
        public string GeneratePassword()
        {
            string PasswordLength = "8";
            string NewPassword = "";

            string allowedChars = "";
            allowedChars = "1,2,3,4,5,6,7,8,9,0";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";


            char[] sep = {  
            ','  
        };
            string[] arr = allowedChars.Split(sep);


            string IDString = "";
            string temp = "";

            Random rand = new Random();

            for (int i = 0; i < Convert.ToInt32(PasswordLength); i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                IDString += temp;
                NewPassword = IDString;

            }
            return NewPassword;
        }

        protected void GetValue(object sender, EventArgs e)
        {
            string strNewPassword = GeneratePassword().ToString();

            RepeaterItem Item = (sender as Button).NamingContainer as RepeaterItem;
            Label mobileno = ((Label)Item.FindControl("Label1")) as Label;
            Label email = ((Label)Item.FindControl("Label2")) as Label;
            Label username = ((Label)Item.FindControl("Label3")) as Label;

            string Uname = username.Text;
            string Email = email.Text;

            SqlConnection con = new SqlConnection(db.connetionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("  update RegisteredUsers SET approvestatus='Approved' , updateddate='" + DateTime.Now.ToString() + "',approverdate='" + DateTime.Now.ToString() + "',password='" + strNewPassword + "' where mobileno='" + mobileno.Text + "'", con);
            cmd.ExecuteNonQuery();

            mail1(strNewPassword, Uname, Email);

            
        }
             
        public int mail1(string pswd, string user, string mailpass)
        {
            int i = 0;


            try
            {
                DateTime dte = DateTime.Today;
                var dt = dte.ToString("dd/MM/yyyy");
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient();
                 mail.From = new MailAddress("mail@brainmagic.info");
                mail.From = new MailAddress("gayathri@brainmagic.info");
                mail.To.Add(mailpass);

                mail.Subject = "Login credentials"; // Mail Subject  

                string body;

                body = "Dear User, <br/><br/>";

                body += "<br/>Please find below Login details";

                body += "<br/>Your username - " + user + " , password -" + pswd;



                body += "<br/>Thank You";

                mail.Body = body;
                //mail.AlternateViews.Add(Mail_Body());
                mail.IsBodyHtml = true;
                //System.Net.Mail.Attachment attachment;
                //attachment = new System.Net.Mail.Attachment(file); //Attaching File to Mail  
                //mail.Attachments.Add(attachment);
                SmtpServer.Host = "webmail.brainmagic.info"; //PORT  
                SmtpServer.Port = Convert.ToInt32(25); //PORT  

                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.UseDefaultCredentials = false;
                // SmtpServer.Credentials = new NetworkCredential("mail@brainmagic.info", "kp0L2r3!");
                SmtpServer.Credentials = new NetworkCredential("gayathri@brainmagic.info", "Gayathri@123");
                SmtpServer.Send(mail);

                
            }
            catch (Exception ex)
            {
                i = 0;
            }
            return i;

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string style = @"<script>.text {mso-number-format:\@;}</script>";
            string file = "sheet";
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=" + file + ".xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            htmlWrite.WriteLine("<br>");
            htmlWrite.WriteLine("<align='right'><Font size='3' Font color='blue'>" + "Date: " + DateTime.Now.ToShortDateString() + "</font>");
            htmlWrite.WriteLine("<br>");
            //htmlWrite.WriteLine("<align='Centre'><Font size='3' Font color='red'>" + "Salary Register for the Month of " + txtMonthYear.Text + "</font>");
            htmlWrite.WriteLine("<br>");
            htmlWrite.WriteLine("<br>");
            rptPriceList.RenderControl(htmlWrite);
            htmlWrite.WriteLine("<br>");
            Response.Write(style);
            Response.Write(stringWrite.ToString());
            Response.End();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
        }

        

       
        

            
        }
        
    }

