﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Masters/Site.Master" CodeBehind="listregisteruser.aspx.cs" EnableEventValidation="false" Inherits="TurboProductCatalogue.RegisterUser.listregisteruser1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    <style type="text/css">
     
.pagination {
  display: inline-block;
}

.pagination a {
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
}

.pagination a.active {
  background-color: #4CAF50;
  color: black;
  border-radius: 5px;
}

.pagination a:hover:not(.active) {
  background-color: #ddd;
  border-radius: 5px;
}
  </style> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i>&nbsp;Registered User
        </h1>
    </section>   
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pnlMSG" runat="server" Visible="false">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblMSG" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>


                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">List of Registered User</h3>
                        <div class="box-tools">
                            <asp:Button ID="Button1" runat="server" class="btn btn-success btn-sm" Text="Download Excel" OnClick="Button1_Click" />
                        </div>
                    </div>
                    <div class="box-body">
                       
                        <div class="table-responsive">
          
                            <asp:Repeater ID="rptPriceList" runat="server" OnItemCommand="rptPriceList_ItemCommand">
                                <HeaderTemplate runat="server">
                                    <table id="tblCustomers"  class="table table-hover table-bordered">
                                        <tr class="success">
                                            <th class="text-center" style="width: 5%">S.No</th>
                                            <th class="text-center" style="width: 15%">Name</th>
                                             <th class="text-center" style="width: 15%">Mobile No</th>
                                             <th class="text-center" style="width: 20%">Email</th>
                                             <th class="text-center" style="width: 20%">State / City</th>
                                             <%--<th class="text-center" style="width: 40%">City</th>--%>
                                             <th class="text-center" style="width: 20%">User Type</th>
                                             <%--<th class="text-center" style="width: 40%">Device Type</th>
                                             <th class="text-center" style="width: 40%">Device Model</th>
                                             <th class="text-center" style="width: 40%">Device Model</th>
                                             <th class="text-center" style="width: 40%">OTP</th>--%>
                                             <th class="text-center" style="width: 40%">Approve Status</th>
                                             <%--<th class="text-center" style="width: 40%">Insert Date</th>
                                             <th class="text-center" style="width: 40%">Update Date</th>--%>
                                              <th class="text-center" style="width: 25%">View Users</th>
                                             <th class="text-center" style="width: 25%">Approve</th>
                                            <th class="text-center" style="width: 25%">Reject</th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="text-center">
                                            <%# Container.ItemIndex +1 %>

                                        </td>
                                        <td>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("username") %>'></asp:Label>
                                           <%-- <%# DataBinder.Eval(Container.DataItem,"username")%>--%>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("mobileno") %>'></asp:Label>

                                           <%-- <%# DataBinder.Eval(Container.DataItem,"mobileno")%>--%>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("email") %>'></asp:Label>
                                            <%--<%# DataBinder.Eval(Container.DataItem,"email")%>--%>
                                        </td>
                                        <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"state")%> /  <%# DataBinder.Eval(Container.DataItem,"city")%>
                                        </td>
                                       <%-- <td class="text-center">
                                          
                                        </td>--%>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"usertype")%>
                                        </td>
                                        <%-- <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"devicetype")%>
                                        </td>
                                         <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"devicemodel")%>
                                        </td>
                                        <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"devicename")%>
                                        </td>
                                        <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"otp")%>
                                        </td>--%>
                                        <td  class="text-center">
                                            <%# ( DataBinder.Eval(Container.DataItem,"approvestatus"))%>
                                        </td>
                                       <%-- <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"inserteddate")%>
                                        </td>
                                        <td class="text-center">
                                            <%# DataBinder.Eval(Container.DataItem,"updateddate")%>
                                        </td>--%>
                                        <td class="text-center">
                                            <a href="viewregisteruser.aspx?mode=edit&userID=<%# TurboProductCatalogue.MyCrypto.GetEncryptedQueryString (DataBinder.Eval(Container.DataItem,"userID").ToString())%>"  class="btn btn-primary btn-xs">View User</a>
      
                                        </td>
                                        <td class="text-center">
                                      
                            <%--<asp:Button ID="Button1" runat="server" Visible='<%# (Eval("approvestatus").ToString() == "Pending") %>'  class="btn btn-success btn-xs" CommandName="Approve" Text="Approve" OnClick="GetValue" />   --%>
                                            <%--<input id="Button1" runat="server" type="submit"  value="button" onclick="GetValue" />   --%>
                                            <asp:Button id="bWLAmend" onclick="GetValue" runat="server" class="btn btn-success btn-xs" EnableViewState="True" Text="Approve" Visible='<%# (Eval("approvestatus").ToString() == "Pending") %>' CommandName="Select"/> 
                                           
                                        </td>
                                         <td class="text-center">
                                             <asp:Panel ID="pnlRejection" Visible='<%# (Eval("approvestatus").ToString() == "Pending") %>' runat="server">
                                                 <a href="rejectform.aspx?mode=edit&userID=<%# TurboProductCatalogue.MyCrypto.GetEncryptedQueryString (DataBinder.Eval(Container.DataItem,"userID").ToString())%>" class="btn btn-danger btn-xs">Rejected</a>
                                             </asp:Panel>
                                             
                                            
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>

                                </asp:Repeater>
                              <div class="pagination">
        <asp:Repeater ID="rptPaging" runat="server" onitemcommand="rptPaging_ItemCommand">
            <ItemTemplate>
                                <asp:LinkButton ID="btnPage" 
                 onmouseover="this.style.background='Green';" onmouseout="this.style.background='white';"
                CommandName="Page" CommandArgument="<%# Container.DataItem %>"  runat="server" ForeColor="Black" Font-Bold="True"><%# Container.DataItem %>
                                </asp:LinkButton>
           </ItemTemplate>
        </asp:Repeater>
        </div>
                     

                          
                            <asp:Panel ID="pnlNoRecord" Visible="false" runat="server">
                                <h3 class="text-danger text-bold text-center">No Record Found</h3>
                            </asp:Panel>
                                </div>
                        </div>
                    </div>




                </div>
            </div>

    </section>
</asp:Content>
