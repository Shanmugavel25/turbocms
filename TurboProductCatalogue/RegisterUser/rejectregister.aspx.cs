﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Drawing;
using System.Text;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TurboProductCatalogue.RegisterUser
{
    public partial class rejectregister : System.Web.UI.Page
    {
        public bool noError = true;
        public common db = new common();

        protected void Page_Load(object sender, EventArgs e)
        {
            string strMSG;
            strMSG = Request.QueryString["msg"];
            if (!string.IsNullOrEmpty(strMSG))
            {
                pnlMSG.Visible = true;
                lblMSG.Text = strMSG;
            }
            if (!IsPostBack)
            {
                getData();
            }
        }
        protected void getData()
        {
            SqlConnection conn = new SqlConnection(db.connetionString);
            SqlCommand Cmd = new SqlCommand();
            //SqlDataReader sqlReader;
            //try
            //{
            Cmd = new SqlCommand("select * from RegisteredUsers where approvestatus='Reject'", conn);
            Cmd.Parameters.Add("@MSG", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
            conn.Open();

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(Cmd);
            adp.Fill(dt);
            PagedDataSource pgitems = new PagedDataSource();
            DataView dv = new DataView(dt);
            pgitems.DataSource = dv;
            pgitems.AllowPaging = true;
            pgitems.PageSize = 15;
            pgitems.CurrentPageIndex = PageNumber;
            if (pgitems.PageCount > 1)
            {
                rptPaging.Visible = true;
                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPaging.DataSource = pages;
                rptPaging.DataBind();
            }
            else
            {

                rptPaging.Visible = false;

            }
            rptPriceList.DataSource = pgitems;
            rptPriceList.DataBind();

            conn.Close();
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            getData();
        }



        protected void rptPriceList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }
    }
}