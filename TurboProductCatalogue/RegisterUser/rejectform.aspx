﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="rejectform.aspx.cs" Inherits="TurboProductCatalogue.RegisterUser.rejectform" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>
            <i class="fa fa-user "></i>&nbsp;User Rejection
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <asp:Panel ID="pnlerror" runat="server" Visible="false">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <div class="box  box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="lblHead" runat="server" Text=""></asp:Label>
                            Rejection Form</h3>   
                        <div class="box-tools">
                            <a href="listregisteruser.aspx" class="pull-right btn btn-success btn-sm">Back</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> Reason For Rejection <span class="text-danger">*</span></label>
                                <asp:TextBox ID="txtremarks"  MaxLength="250" TextMode="MultiLine" Width="350" CssClass="form-control" placeholder="Enter Remarks" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqTitle" Display="Dynamic" ForeColor="Red" ControlToValidate="txtremarks" runat="server" ErrorMessage='Enter Remarks'></asp:RequiredFieldValidator>
                            </div>
                        </div>
                      
                        </div>
                    <div class="box-footer">
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="btn btn-success pull-right" Text="Submit" />
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
